This chapter explains the other half of the program duo mention in the begining
of the previous chapter, the alignment process.\\
\section{Aligning the query}
The process of aligning a DNA sequence to a genome is somewhat tedious, as most
of the steps required are strictly linear, all depending on previously made
computations. Thusly, both vectorization and parallelization were kept in mind
together with cache limitations when the prototype were created.

\subsection{Searching for the best location} \label{IMP:search}
The first thing that were established were the general flow of the program, in
order to analyze the potential for parallelization. As seen from figure
\ref{figure:original_flow}, there are 3 main parts to the program, finding
potential matches, aligning them with the Smith-Waterman algorithm, and then
providing output in the form of a SAM file.\\
\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{General_Flow.png}}
            \caption{General program flow}
            \label{figure:general_flow}
\end{figure}
The alignment program created in this thesis had to utilize the Xeon Phi to the
fullest and in order to do so, a new program flow
were thought to provide the most balanced outcome while still
also easily amended if the balance are found to be skewed.
An initial threading, splitting the program in two, allows both reading and process
two queries at the same time. After the first search procedure is complete, one
thread will read and process the next query, if any, and the other will
calculate Smith-Waterman to determine the best location before writing the
results to an output file. Additionally, if the initial search to find possible
matches results in a perfect match, there will be no need to calculate SW and
thusly skipping ahead directly to the output section.

\subsubsection*{Finding possible match locations}
The first step when processing the query is to compress the query in the same
manner as the L column as well as creating a compressed version of the
reverse-complimented query as well. The reverse-compliment is created to combat
the fact that a DNA string itself is originally paired with its reverse
complimented. An example; the reverse-compliment of $CANTGAT$ by following the
conversion table, see table \ref{table:rc}, is $ATCANTG$. The process involves
starting at the back of the query and taking the complimented and placing it
first, and so on until the whole original query have been changed.
\begin{table}[!h]
    \begin{center}
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        A & C & G & T & N \\
        \hline
        T & G & C & A & N \\
        \hline
    \end{tabular}
    \caption{Reverse-Compliment table}
    \small{Conversion from top to bottom.}
    \label{table:rc}
\end{center}
\end{table}
After creating the two compressed query strings, both are processed in turn by
beginning at the end of the string and matching sequential characters forward
in the CRS by following the formula, presented in figure \ref{figure:search},
until no match is possible or the end of the query have been reached. The
resulting (sub)string is stored in a list sorted by match length and if there
are any characters left in the query, the process starts anew from the location
where it left of. If an N or the end of line character is encountered, an
automatic mismatch occur.
\begin{figure}[!h]
    \begin{center}
        \begin{framed}
        \caption{FM index - search equation}
        \label{figure:search}
        \small{
\begin{verbatim}
search range given by:
    (tmp_)St = start index
    (tmp_)Ed = end index

length = how many characters matched

for (each char in query, reversed order)
    if (char == N || end_of_string)
        if (length > 0)
            store_found(St, Ed, length)

    if (first)
        St = index_table[char][0]
        Ed = index_table[char][1]
    else
        tmp_St = index_table[char][0] + rank_table[char][St-1]
        tmp_Ed = index_table[char][0] + rank_table[char][Ed] - 1

    if (tmp_St > tmp_Ed)
        \\missmatch found
        if (length > 0)
            store_found(St, Ed, length)
            make sure for loop starts again at current char.
            length = 0
    else
        St = tmp_St
        Ed = tmp_Ed
        length++

After for loop compleation
if (length > 0)
    store_found(St, Ed, length)
\end{verbatim}
}
        \end{framed}
    \end{center}
\end{figure}

When storing only the 1600 best matches are kept for further processing, and if
a match is found to be crossing over from one chromosome to another the match
is dropped altogether.

\subsubsection*{Speed vs. Accuracy}
When calculating the possible matches for the two queries, a choice had to be
made on how through the matching process should be. One could start a new match
sequence for each character in the query, resulting in a small possible
increase in accuracy while suffering speed loss and several almost equal
matches. Another option were to thread the search procedure, starting going
through the FM-index on an individual basis. After the initial step of setting
St and Ed when length is 0 from figure \ref{figure:search}, spawning a thread
that would utilize the search procedure explained in section \ref{TA:FMI} for
each possible start location, and then spawn a new thread for each possible
forked road encountered. This were quickly discarded as, if assumed equal
distribution of A, C, G and T, a potential of 750000 threads could be spawned
from the initial thread spawn alone.

\subsection{Calculating Smith-Waterman}
Even though Smith-Waterman is a strictly serial calculation equation, by
utilizing the vector unit on the Xeon Phi, 16 simultaneously calculations were
achieved, where the calculations occur on a column basis of the Smith-Waterman
matrix. This were inspired from the Rognes' SWIPE\cite{swipe} implementation
and it also portrayed a way of only keeping two columns in memory at the same
time. As the 1600 best matches were kept, the whole Smith-Waterman calculation
could also be calculated in parallel, spawning 100 threads to calculate the
match score of 16 possible locations each, all at the same time.
The slightly modified SWIPE Smith-Waterman implementation can be seen in figure
\ref{figure:modSW}. The function requires several input to function properly;
\begin{itemize}
    \item A pointer to the memory area (HE) for storing two columns worth of data
        to keep track of the calculated values and the cumulative row gap
        penalties of the SW calculation matrix.
    \item A pointer to the memory area (sp) that holds information on whether the
        reference match is from the regular or the reverse-complimented query.
        This memory area should also holds the results after calculation.
    \item A char array (q) holding the query bundle, as explained by figure
        \ref{figure:QueryMemoryLayout}.
    \item The length of the query itself (q\_len).
    \item An indication as to how many different reference positions it should
        match the query to (multiple), ranging from 1 to 16 depending on native
        vs host execution and or uneven results from the previous step to find
        possible locations.
    \item An index (si) to let the function know where in sp the current
        execution should located and then store information to/from.
    \item A pointer to the memory area that holds the uncompressed reference
        sequences it should match the query against.
\end{itemize}
The last input parameter were not required in the first version of the function
as it then read the references from the compressed reference in the CRS from
locations found in sp. The choice to change the algorithm were made to combat
the poor cache capabilities of the Xeon Phi and will be explained later in
section \ref{sec:repack}.\\

There are also several local variables created for each instance of the
function;
\begin{itemize}
    \item int i and j to loop through the columns and rows of the SW
        calculation matrix, respectively.
    \item int t to iterate through the different reference sequences when
        acquiring the cell score.
    \item and unsigned int r\_len to hold the length of the reference sequences
        to match against. This length is the same as the length of the query
        with an extra few characters added before and after the match to allow
        for some adjustment to the initial partial match found.
    \item vector variables S and SM to hold the highest score found for each
        reference as well as the score for the currently calculated cell,
        respectively.
    \item the H vector variable, initially holds the diagonally cell value
        before moving on to become the current cell after added with the
        scores (SM).
    \item lastly, the vector variables E, F and N each hold the values of
        the cell to the left, cells above and the cells diagonally up to the
        left, respectively. The E and F variable holds the same purpose as
        described in Gotohs improved SW algorithm, section \ref{Op:Gotoh}, seen
        in figure \ref{figure:GOTOH}. The N variable help H to fulfill its role
        according to the Gotohs improved SW algorithm by holding the value of
        the cell to the left.
\end{itemize}

First thing that the function does is to make sure that both the HE array and
the max score found is set to 0 as this is the initial values of all SW
calculations. It then proceeds to the double for loop to do the actual
calculations. Before staring to calculate a new column, both H and F are set to
zero to fulfill initial values as if the calculations had been done utilizing
the whole matrix. Then before each cell calculation, N and E are updated
according to the HE array before storing the cell scores for each reference
match into the SM vector. The latter had to be done in a for loop as there were
no intrinsic support on the Xeon Phi to load a 512 vector with 16 individual
integers, before casting the results into the vector.
The first set of intrinsic calls are taken straight from the algorithm itself,
first adding the score to the diagonal cell value and then finding the highest
value among the result, E, F or 0 to store in H. If the new H values exceeds
any of the values in S, they replace those values in S to always have the
highest cell score stored in S.
The final step performed when calculating a cell is to prepare for the next
cell by updating the E and F values before updating the HE array.
After all cells have been updated a simple for loop withdraws the maximum
scores for each reference sequence and stores them in their respective slots in
the storage area.
\begin{figure}[!h]
    \begin{center}
        \begin{framed}
            \caption{Modified Smith-Waterman implementation}
            \label{figure:modSW}
\begin{verbatim}
void sw_simd(...) {
    int score_results[16];
    unsigned int i, j, t;
    unsigned int r_len = q_len + (2*SW_REF_PADDING);

    __mxxxi H, E, F, N, SM, S;

    S = *(__mxxxi *)ZERO;

    for (i = 0; i < r_len; i++) {
        H = *(__mxxxi*)ZERO;
        F = *(__mxxxi*)ZERO;

        for (j = 0; j < q_len; j++) {
            N = HE[j*2 + 0];
            E = HE[j*2 + 1];

            for (t = 0; t < multiple; t++)
                fill score_results with matching scores.

            SM = *(__mxxxi*)score_results;

            H = _simd_add_epi32(H,SM);
            H = _simd_max_epi32(H,F);
            H = _simd_max_epi32(H,E);
            H = _simd_max_epi32(H,*(__mxxxi*)ZERO);
            S = _simd_max_epi32(H,S);

            HE[j*2 + 0] = H;

            E = _simd_sub_epi32(E,*(__mxxxi*)GE);
            F = _simd_sub_epi32(F,*(__mxxxi*)GE);
            H = _simd_sub_epi32(H,*(__mxxxi*)GOE);
            E = _simd_max_epi32(H,E);
            F = _simd_max_epi32(H,F);

            HE[j*2 + 1] = E;
            H = N;
        }
    }

    for (i = 0; i < multiple; i++) {
        sp[si+i].score = ((int *)&S)[i];
    }
}
\end{verbatim}
        \end{framed}
    \end{center}
\end{figure}

\subsection{Repacking the reference genome} \label{sec:repack}
In order to decrease the amount of cache lines needed to be read in the
Smith-Waterman implementation, the possible locations used in one run of the
algorithm were repacked into a single string. The initial implementation relied
on the whole reference string and indexes to retrieve the information stored
within. This resulted in a new cache line being read almost every time, up to
16 times per round in the inner loop of the implementation, as the
sections in the reference string being mapped are most likely positioned in
different locations. The new implementation were to withdraw the information
before the calculation and then merging those into a single char array that the
algorithm could use instead. The merging happens one section at a time to
prevent all of the cache line reads to move outside instead of being removed.
Each character in the section to be merged, are placed at an equal interval in
the new string where the starting location increases once for each section
added. An example of this merge can be seen in figure \ref{figure:merge}, and
resulted in far less cache lines being read as all the data for the reference
sequences were now aligned in memory with the correct order.

\begin{figure}[!h]
    \begin{center}
        \begin{framed}
            \caption{Reference merge repacking}
            \label{figure:merge}
            \begin{tabular}{c}
            An example showcasing the merging of four sequences\\
            into a single string:\\
            \\
                \begin{tabular}{cccccc}
                    $s_{1}(GNAT)$ & - & $G_{1,1}$ & $N_{1,2}$ & $A_{1,3}$ & $T_{1,4}$ \\
                    $s_{2}(ACAT)$ & - & $A_{2,1}$ & $C_{2,2}$ & $A_{2,3}$ & $T_{2,4}$ \\
                    $s_{3}(CANT)$ & - & $C_{3,1}$ & $A_{3,2}$ & $N_{3,3}$ & $T_{3,4}$ \\
                    $s_{4}(AGAT)$ & - & $A_{4,1}$ & $G_{4,2}$ & $A_{4,3}$ & $T_{4,4}$ \\
                \end{tabular} \\
                $\Downarrow$ \\
                $G_{1,1}$ $A_{2,1}$ $C_{3,1}$ $A_{4,1}$ $N_{1,2}$ $C_{2,2}$ $A_{3,2}$ $G_{4,2}$ $A_{1,3}$ $A_{2,3}$ $N_{3,3}$ $A_{4,3}$ $T_{1,4}$ $T_{2,4}$ $T_{3,4}$ $T_{4,4}$\\
                \\
            \end{tabular}
            Where the lower case numbers symbolizes originating string and
            index within that string, respectively.
        \end{framed}
    \end{center}
\end{figure}

\subsection{Providing output}
For each query provided, only the best match found will be written to the SAM file.
The SAM file output for each query, provided by this program, will only contain
the mandatory 11 fields as well as a 12th field to provide the aligners own
alignment score. The filed data presented in table \ref{table:ExpexcteOutput}, are meant to illustrate what
data to expect from the aligner.
\begin{table}
\begin{center}
    \begin{tabular}{|l|l|}
        \hline
        QNAME & A maximum of 20 of the first character in the query ID given as input.\\
        FLAG & Only the reverse-complimented bit will be used.\\
        RNAME & Will contain the chromosome name of the alignment; 1-22, X, Y, MT or Non.\\
        POS & 1-based index of the leftmost mapped character.\\
        MAPQ& The default value of 255 will be used for this field.\\
        CIGAR& The CIGAR string contain the following characters: M, D, I and S.\\
             & They will be written in a compressed manner, such as MMM will be 3M.\\
        RNEXT & Default value * will be used. \\
        PNEXT & Default value of 0 will be used.\\
        TLEN & Default value of 0 will be used.\\
        SEQ & The sequence mapped to.\\
        QUAL & Default value of * will be used.\\
        AS:i: & The actual score returned by SW.\\
        \hline
    \end{tabular}
\end{center}
\caption{Expected values in SAM output}
\label{table:ExpexcteOutput}
\end{table}
