\documentclass[USenglish]{ifimaster}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc,url}
\urlstyle{sf}
\usepackage[english]{babel}
\usepackage{textcomp,csquotes,ifimasterforside,varioref,graphicx,amsmath,framed}
\usepackage[hidelinks]{hyperref}
\usepackage[backend=biber,style=numeric-comp,maxbibnames=10,minbibnames=10]{biblatex}
\setlength{\parindent}{0pt}

\title{A Journey to the core}
\subtitle{of man and machine alike}
\author{Reidar André Brenna}

\bibliography{Master.bib}

\begin{document}
\ififorside{}
\frontmatter{}
\maketitle{}

\chapter*{Abstract}
This thesis tries to determine if the Xeon Phi co-processor released in 2013 is
a suitable hardware tool to perform DNA sequence alignments on.
Starting off with an exploratory dive into the hardware itself before tackling
some of available optimization techniques and previously implemented tools.
As there are no tools currently on the market that utilizes the Xeon Phi
hardware, the only comparisons made are to a regular Xeon processor to
determine the outcome.
Two programs were made in the course of this thesis, CRSbuild and DNAlign, that
builds an FM index over the human genome and aligns sequences to the genome
respectively. The results achieved in this thesis are rather disappointing and
the thesis shows, to some degree, that native execution of an alignment tool of
this magnitude is not suited. The shared memory architecture of the Xeon Phi
posses a relatively small combined cache and with the lack of
support for smaller data types this is a limitation that the four hardware
thread and a 512 bit vector unit per core can not overcome.

\section*{Acknowledgments}
I would like to thank my supervisors, Torbjørn Rognes and Johan Hjelmervik;
without their input I would probably not have made it as far as I did.
I will also give a huge thanks to Jorun Ramstad, you kept my motivation up when
the work seemed to much and for her help with some of the figures\\

Finally I would like to thank my loving wife for her understanding and patience
during this master thesis.

\tableofcontents{}
\listoffigures{}
\listoftables{}

\mainmatter{}

\part{Introduction}
\chapter{Introduction}
\section{Motivation}
Aligning DNA Sequences to a genome is an important and time-consuming task in
bioinformatics as it is the first step taken in order to analyze DNA samples.
By aligning the sequences to a genome, one may gain several important clues
about the health of the donor.

In January 2013, Intel released the Xeon Phi co-processor as a serious contender
in the high performance programming field. It is designed to tackle highly
parallel problems, and is a promising hardware to implement a sequence
alignment tool on. However promising it may be, the challenge to find a
suitable technique to take advantage of the unique hardware is highly present
as there are no other program that has yet to take advantage of the Xeon Phi
when mapping DNA sequences. An examination of the co-processor itself, as well
as some of the tools implemented for other kinds of hardware is needed in order
to attempt this task.

\section{Goal}
The Goal of this thesis is to determine whether the new Intel Xeon Phi
co-processor is suitable to perform DNA sequence alignment, and in the process
attempt to implement one such program.

\chapter{Background}
\input{XeonPhi.tex}
\input{DNASequenceMapping.tex}
\input{Optimization.tex}
\input{ToolsAlgorithms.tex}

\part{Implementation}

\chapter{Startup}
\input{Startup.tex}

\chapter{Working with the human genome}
\input{WorkingWithTheHumanGenome.tex}

\chapter{Searching for a needle} \label{ch:needle}
\input{SearchingForANeedle.tex}

\chapter{Tuning the application}
There are a few options available to tune the alignment program, and will
be explained in this chapter.

\section{Runtime tuning}
There are several command line parameters available when executing DNAlign,
the alignment program as seen in table \ref{table:DNAlign_parameters}
\begin{table}[!h]
    \begin{center}
        \begin{tabular}{|ll|}
            \hline
            <ref> & the compressed reference file (.crs) created by CRSBuild\\
                  & one reference file only\\
            \hline
            $[$query$]$ & $[0-N]$ query files\\
                        & All parameters given after <ref> are assumed to be\\
                        & query files that follows the FASTQ format \\
            \hline
            flags:  & (provided before <ref> if any)\\
                    &
            \begin{tabular}{lcl}
                -h & & Prints the help menu and terminates the execution \\
                -p & & Prints the content of the .crs file to debug.out \\
                   & & - not recommended \\
                -n & <int> & Number of threads to use\\
                   &       & - Default value: 1\\
                -m & <int> & Number of matches to write to SAM file\\
                   &       & - Default value: 1\\
                -l & <int> & Minimum length of partial match to keep \\
                   &       & - Default value: 20\\
                   &       & - Higher value results in longer matches\\
                   &       & - Lower value results in shorter matches\\
            \end{tabular} \\
            \hline
        \end{tabular}
        \caption{DNAlign command line overview}
        \label{table:DNAlign_parameters}
    \end{center}
\end{table}
In addition, there are also possible to change the accepted query length which
is set to 200 that requires recompilation to function. There are also provided
a Makefile that have two main features; make all and make phi, which builds the
program to execute on a Xeon Processor or the Xeon Phi processor respectively.

\section{Memory usage}
As the available memory is quite limited on a Xeon Phi, a higher query length
than 200 is not advisable on the low end Xeon Phi co-processor, and the program
have not been tested on such. The program aim to utilize all available memory
when executed with 200+ threads.

\section{Threading}
The thread option available to the user as a command line argument is the total
thread count used by the program. Thusly, if more than one thread is given, one
of these will be taken by the main program to run two query processing
processes simultaneously.

\section{Vectorization}
Vectorization is automatically changed to match the vector capabilities of a
Xeon processor or the Xeon Phi co-processors when built. As these two hardware
units does not share any vector intrinsics, the Xeon processor is thusly only
capable of performing 4 Smith-Waterman calculations at the same time while the
co-processor handles 16 simultaneously.

\part{Conclusion}
\chapter{Results}
\section{Test conditions}
As there are no other program that aligns DNA sequences to a reference genome
developed for a Xeon Phi, the testing and comparison were done on a Xeon
processor and the co-processor to gage the effectiveness of the Xeon Phi
implementation.
Due to the fact that a lot of time went into getting the Xeon Phi to function
properly in the first half of this thesis, and the results yielded, accuracy
have not been compared to other programs that does not utilizes the Xeon Phi.
The tests were performed 10 times each to properly calculate a mean time for
comparison.\\

Note: Due to unforeseen events and hardware malfunction in the testing phase,
the tests were only performed with a single thread and two queries against 5
relatively small chromosomes instead of the whole genome to save time.

\section{Testing}
\subsection*{Native}
These are the following test times achieved on the Xeon phi when aligning 5
almost perfect matches to chromosome 15.
\begin{tabular}{|c|c|c|c|c|}
    5.951 & 0.842 & 0.769 & 0.746 & 0.772 \\
    0.778 & 0.734 & 0.731 & 0.742 & 0.727 \\
\end{tabular}

\subsection*{Host}
These are the following test times achieved on the Xeon phi when aligning 5
almost perfect matches to chromosome 15.
\begin{tabular}{|c|c|c|c|c|}
    0.155 & 0.141 & 0.121 & 0.168 & 0.200 \\
    0.170 & 0.121 & 0.207 & 0.124 & 0.141 \\
\end{tabular}

\subsection{Comparison}
Even though with relative small test samples it is quite clear that the host
execution were significantly faster than the native execution.\\
Even though the Xeon Phi executes the Smith-Waterman calculation 4 times less
than the Xeon processor it is about 5 times slower than the Xeon in runtime.
As the thesis discovered that every time a series of calculations were
performed on the Xeon Phi, the first time were always much higher than the
rest, the following stats have been calculated when disregarding the highest
and lowest runtime of both hardwares.\\
The mean execution time for the Xeon Phi co-processor were 0.7655 seconds while
the mean time for execution on a Xeon processor were only 0.1525 seconds.
Thusly, it becomes clear that without any threads the Xeon Phi is the tortoise
to the Xeon Hare.

\chapter{Future work}
There are several things that this thesis did not get a chance to test properly
and this chapter is dedicated to things that could be done in the future.

\section{Hardware}
Intel have stated that they are releasing a newer version of the Xeon Phi with
more internal memory and cache as well as support for smaller unit sizes in the
vector unit. However, the first iterations of the new Xeon Phi will not have
the opportunity to communicate with the host CPU and thusly and offloading
implementation on the new hardware will have to wait on the next release after
the that.
The hardware does however propose the opportunity to test this program again
with more memory and less compression to remove several steps in the process
when searching CRS structure.

\section{Application improvements}
This thesis did not get the opportunity to test an Offloading approach on the
implementation, and that could be a viable solution to take the advantage of
the host processor for the tedious linear work and only offload the
Smith-Waterman calculations to the co-processor. This could provide beneficial
as long as the overhead associated with the offload does not take to much
time.\\
The general idea behind the offload approach is to allocate the memory space
needed on the co-processor in the very beginning of the execution and keeping
the memory allocated until program termination. The general Flow of the program
would then be that one or more threads on the host CPU read input, found
possible matches and created the repacked reference sequence before shipping it
and the query to the co-processor to calculate the Smith-Waterman scores. After
the scores had been calculated, they would be returned to the host processor
that writes the findings to the SAM output file.\\
Regarding thread synchronization, the initial thought would be to only allow
one of the host CPU threads to write to the file at the time by wrapping that
part in a critical section. This would also have to be done when reading the
query, to prevent multiple threads to ruin queries for each other.
Based on the total thread, cache and memory availability on the co-processor,
only two offloads to the co-processor at the same time would be optimal. This
could of course be proven wrong by an implementation and proper testing.

\chapter{Final verdict}
As the co-processor is today, the low end at least, this thesis have found it
to be less than preferable when aligning DNA sequences to a reference genome in
Native mode. With more testing and perhaps some fine tuning and an offload
approach to the implementation it could have proven better than the native
execution and perhaps also better than some of the other tools on the market
today. Although the latter is less likely.

\backmatter{}
\printbibliography
\end{document}
