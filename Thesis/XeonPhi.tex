%%Xeon Phi - Background information

\section{Xeon Phi}\label{XeonPhi}
The Intel Xeon Phi co-processor, although relatively new
compared to traditional GPUs, have made a big impact on the TOP500 list of
supercomputers and are gaining traction in HPC-systems rapidly <CITE top500?>.
The co-processor contain roughly 60 cores clocked at
1GhZ each with a shared memory area and a 512-bit wide vector unit, located on
a PCI board for easy assembly and is the chosen hardware for this thesis.
The co-processors will be described in short in the following subsections as
stated in its Best practice guide\cite{xeonguide} and the book \emph{Intel Xeon
Phi Coprocessor High Performance Programming}\cite{xeonintel}

\subsection{Specifications}\label{XeonPhi:Spec}
\subsubsection*{Overview}
The co-processor, spotting a full service Linux operating system with support
for many of Intels' own development tools in addition to OpenMP, C/C++ and MPI,
is designed to work, alone or several together, along side the Intel Xeon
processor, connected to the host through the PCI express bus.
The operative system, designed for a Many Integrated Core architecture (MIC),
allows a user to execute code directly on the co-processor
in addition to offloading whole, or parts of, programs from the host CPU.
The cores shares the same fundamentals as the original Pentium core and are
thusly in-order dual issued x86 cores. Consisting of two processing units; the
scalar unit and the expanded vector unit spotting a whole new vector
instruction set and the 512-bit wide vector unit. Each core is able to fetch
and decode instructions from up to four hardware threads and, due to the double
processing unit, execute two of them per cycle.
Each core, as shown in figure \ref{coreOverview}, is connected by a high
performance on-die bidirectional ring interconnect, the Core Ring Interface
(CRI), and has equal access to a shared memory and all I/O devices connected to
the host computer.
The co-processor also comes with several auto-vectorization possibilities
enabled by default, and several pragma compiler directives may be used for
further customization of Xeon Phi code.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.8]{core.png}}
\caption[A Xeon Phi Core]{Architecture of a single Xeon Phi core drawn from the
    figure in \emph{Intel Xeon Phi Coprocessor High Performance
Programming}\cite{xeonintel}.\label{coreOverview}}
\end{figure}

A simplified version of the architecture is depicted in figure
\ref{architecture} with only 4 cores visible on the CRI together with the L2
cache, memory ports and tag directory (TD) loosly drawn from \emph{Intel Xeon Phi Coprocessor High Performance
Programming}\cite{xeonintel}.

\begin{figure}[!h]
\centerline{\includegraphics[scale = 1]{mic.png}}
\caption[Architecture overview]{Simple architecture sketch based on figure in \emph{Intel Xeon Phi Coprocessor High Performance
Programming}.\cite{xeonintel}\label{architecture}}
\end{figure}


\subsubsection*{VPU}
The Vector Processing Units (VPU) new vector instruction set provide a
plethora of new vector intrinsics to the user, making it possible to vectorize
algorithms such as Smith Waterman.
As a rule of thumb when utilizing such instructions, regarding execution time,
is that they all have a latency to throughput ration of 4 to 1 cycles.
As the vector unit requires a 64.byte alignment of its content, non of Intel's
previous vector intrinsics are supported, but it comes bundled with an extended
math unit (EMU), making it possible to execute 16 single-precision or 8
double-precision operations simultaneously.

\subsubsection*{Memory}
There are eight memory controllers on a Xeon Phi, supporting up to 16 GDDR5 channels.
A transfer speed of up to 5.5 GT/s, with each transfer being 4 bytes of data,
and a theoretical aggregated bandwidth ranging from 240 to 352 GB/s is provided
for fast memory access.
The cores access the memory through the CRI with hook memory controllers
located on the die and by linking the memory to the ring a smoother memory
operation is achieved when all cores are utilized.

\subsubsection*{Cache}
Each core posses a L1 cache consisting of a 32 KB instruction cache and 32 KB
data cache with a cache lines of size 64 byte exclusively accessible only to
the core itself.
The L1 load-to-use latency is 1 cycle, e.g. an integer value loaded can be used on the next cycle by an integer
instruction. (Vector instructions operates with a different latency).
The cores also contribute 512 KB to the shared global L2 cache storage
and if no core shares any code or data the effective size of the L2 cache
storage is up to a total of 31 MB. However, as the L2 cache size is highly
dependant on the degree of shared code and data among the cores, the size could
also be, if every core share the same code and data, closer to 512 KB.
Similar to the L1 cache, L2 cache also have a 64 byte cache line but the cycle
latency of the L2 cache is higher with its clock cycle latency of 11 cycles.
L2 cache also supports ECC correction and come with a streaming hardware prefetcher.

\subsection{Xeon Phi Software Development}\label{XeonPhi:SD}
As mentioned in \ref{XeonPhi:Spec} Overview, the Xeon Phi allows the programmer to
develop code that could be executed natively without the need of host CPU
involvement in addition to more traditional offloading paradigms.

The first, native programming, are developed to run on the Xeon Phi without
involvement from the CPU. These applications need to be highly parallelized
with little to none serial dependency to utilize the thread possibilities of the
co-processor without having parts of the execution waiting for other threads to
be completed. It should also utilize the vector unit to the fullest as this is
one of the co-processors biggest strengths, if not the biggest. A Native
program has the added benefit of removing the overhead associated with data
transferal from host CPU to processing unit. On the other hand, a native
program should refrain from overuse of I/O calls as this is
significantly slower on Xeon Phi compared to the Xeon CPU in addition to
a limited amount of accessible memory.

The second, the offload model, comes in several intensities. An application
that have distinct parts of highly parallelize -and vectorize-able code could
benefit greatly from offloading those parts to the co processor if the overhead
associated with the transferal could be kept as small as possible.
Intel has also provided in addition to communication protocols such as OpenMP
and MPI compiler directives to allow a software developer to pass information
between host CPU and co-processor seamlessly throughout the code with options
to both chose whether the co-processor should allocate and or free memory when
called or to reuse memory already present in its shared memory.

\subsubsection*{Optimization}
When developing an application that executes, wholly or partially, on the Xeon
Phi co-processor there are a few important aspects to take into consideration
in order to achieve optimal performance.
The book \emph{"Intel Xeon Phi Co-processor High Performance
Programming"}\cite{xeonintel}, written by Jeffers and Reinders, state that
there are three important considerations to take into consideration to gain
the desired high performance the co-processor is capable of deliver: Memory
usage, Scaling and Vectorization.
\begin{center}
\emph{"Trying to use Intel Xeon Phi co-processor without having maximized the
use of parallelism on the Intel Xeon processor will almost certainly be a
disappointment"}
\end{center}

If the memory usage of the applications is less then the maximum available on a
Xeon Processor, you would probably not gain to much by changing to the
co-processor.\\

High scaling on multiple cores is advisable for any applications that intends
to utilize the co-processor to the fullest.
This may be easily gaged for an application by allowing the application to run
several times with increasingly more threads allowed. By tracking the thread
affinity we'll be able to detect any significant changes in performance and by
some small modifications one may be able to increase performance with more
cores.\\

The last consideration is the ability to vectorize properly. By executing the
application with and without vectorization, the execution without should have a
significant decrease in performance as the co-processor does its best work when
as many cores as possible execute a vector operation every
cycle.\cite{xeonintel}\\

As stated above, a co-processor core is clocked at only 1GhZ and are thusly
slower than a Xeon core without the luxury of branch predictions and
out-of-order execution. To fully utilize each clock cycle to its max
capabilities precautions must be made to avoid unnecessary operations. Due to
the fact that all memory read and writes on the co-processor are performed on
64-bit aligned data, unaligned data would slow down the execution drastically
since the compiler would have to realign the data each time the code requires
it. In some cases where the compiler is unable to catch unaligned data the
application could in worst case terminate itself prematurely due to memory
access errors. Too combat this, it's advisable to use Intels' provided aligned
allocation functions \_mm\_malloc and \_mm\_free instead of regualar malloc and
free. This would ensure that the data is 63-bit aligned and handleable by the
co-processor. To further increase performance one could in addition to the
aligned allocation explicitly tell the compiler that arrays used by the program
is aligned with \#pragma vector aligned to prevent the compiler to include
alignment checks on already aligned data structures.

Another useful tip is to actively try to arrange data in such a way that data
used in succession of each other lies after one another in memory. This could
significantly reduce cache misses and unnecessary cache reads since the L2
cache is both slow and relatively small as previously mentioned.
This comes into play when you execute a highly threaded application running
heavy vector operations.

Following is an extracted checklist for Xeon Phi development:
\begin{itemize}
    \item 64-bit aligned memory
    \item Memory arrangement (cache)
    \item Vectorization
    \item Good usage of compiler directives.
\end{itemize}
