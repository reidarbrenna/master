\BOOKMARK [-1][-]{part.1}{I Introduction}{}% 1
\BOOKMARK [0][-]{chapter.1}{Introduction}{part.1}% 2
\BOOKMARK [1][-]{section.1.1}{Motivation}{chapter.1}% 3
\BOOKMARK [1][-]{section.1.2}{Goal}{chapter.1}% 4
\BOOKMARK [0][-]{chapter.2}{Background}{part.1}% 5
\BOOKMARK [1][-]{section.2.1}{Xeon Phi}{chapter.2}% 6
\BOOKMARK [2][-]{subsection.2.1.1}{Specifications}{section.2.1}% 7
\BOOKMARK [2][-]{subsection.2.1.2}{Xeon Phi Software Development}{section.2.1}% 8
\BOOKMARK [1][-]{section.2.2}{DNA sequence mapping}{chapter.2}% 9
\BOOKMARK [2][-]{subsection.2.2.1}{Illumina}{section.2.2}% 10
\BOOKMARK [2][-]{subsection.2.2.2}{File format}{section.2.2}% 11
\BOOKMARK [1][-]{section.2.3}{Optimization approaches}{chapter.2}% 12
\BOOKMARK [2][-]{subsection.2.3.1}{Dynamic Programming}{section.2.3}% 13
\BOOKMARK [2][-]{subsection.2.3.2}{Smith-Waterman}{section.2.3}% 14
\BOOKMARK [2][-]{subsection.2.3.3}{Parallelization}{section.2.3}% 15
\BOOKMARK [1][-]{section.2.4}{Tools and Algorithms}{chapter.2}% 16
\BOOKMARK [2][-]{subsection.2.4.1}{BWT}{section.2.4}% 17
\BOOKMARK [2][-]{subsection.2.4.2}{FM index}{section.2.4}% 18
\BOOKMARK [2][-]{subsection.2.4.3}{BWA}{section.2.4}% 19
\BOOKMARK [2][-]{subsection.2.4.4}{SOAPv2}{section.2.4}% 20
\BOOKMARK [2][-]{subsection.2.4.5}{BOWTIE}{section.2.4}% 21
\BOOKMARK [2][-]{subsection.2.4.6}{SWIPE}{section.2.4}% 22
\BOOKMARK [-1][-]{part.2}{II Implementation}{}% 23
\BOOKMARK [0][-]{chapter.3}{Startup}{part.2}% 24
\BOOKMARK [1][-]{section.3.1}{Initial setbacks}{chapter.3}% 25
\BOOKMARK [2][-]{subsection.3.1.1}{Hardware cooperation}{section.3.1}% 26
\BOOKMARK [2][-]{subsection.3.1.2}{Software acquisition}{section.3.1}% 27
\BOOKMARK [2][-]{subsection.3.1.3}{Achieving communication}{section.3.1}% 28
\BOOKMARK [0][-]{chapter.4}{Working with the human genome}{part.2}% 29
\BOOKMARK [1][-]{section.4.1}{Implementation}{chapter.4}% 30
\BOOKMARK [2][-]{subsection.4.1.1}{Referencing the genome}{section.4.1}% 31
\BOOKMARK [2][-]{subsection.4.1.2}{From FASTQ to CRS}{section.4.1}% 32
\BOOKMARK [0][-]{chapter.5}{Searching for a needle}{part.2}% 33
\BOOKMARK [1][-]{section.5.1}{Aligning the query}{chapter.5}% 34
\BOOKMARK [2][-]{subsection.5.1.1}{Searching for the best location}{section.5.1}% 35
\BOOKMARK [2][-]{subsection.5.1.2}{Calculating Smith-Waterman}{section.5.1}% 36
\BOOKMARK [2][-]{subsection.5.1.3}{Repacking the reference genome}{section.5.1}% 37
\BOOKMARK [2][-]{subsection.5.1.4}{Providing output}{section.5.1}% 38
\BOOKMARK [0][-]{chapter.6}{Tuning the application}{part.2}% 39
\BOOKMARK [1][-]{section.6.1}{Runtime tuning}{chapter.6}% 40
\BOOKMARK [1][-]{section.6.2}{Memory usage}{chapter.6}% 41
\BOOKMARK [1][-]{section.6.3}{Threading}{chapter.6}% 42
\BOOKMARK [1][-]{section.6.4}{Vectorization}{chapter.6}% 43
\BOOKMARK [-1][-]{part.3}{III Conclusion}{}% 44
\BOOKMARK [0][-]{chapter.7}{Results}{part.3}% 45
\BOOKMARK [1][-]{section.7.1}{Test conditions}{chapter.7}% 46
\BOOKMARK [1][-]{section.7.2}{Testing}{chapter.7}% 47
\BOOKMARK [2][-]{subsection.7.2.1}{Comparison}{section.7.2}% 48
\BOOKMARK [0][-]{chapter.8}{Future work}{part.3}% 49
\BOOKMARK [1][-]{section.8.1}{Hardware}{chapter.8}% 50
\BOOKMARK [1][-]{section.8.2}{Application improvements}{chapter.8}% 51
\BOOKMARK [0][-]{chapter.9}{Final verdict}{part.3}% 52
