%%Sequence Mapping - Background information

\section{DNA sequence mapping}\label{DNAsm}
The process of mapping or assembling DNA sequences into a genome is a vital
addition to DNA sequence technology as the sequencers themselves are unable to
process the entire genome in one go. Varying on the sequencer used, genome
piece size processed have a size ranging from only 20 bases up to 30000 bases.
However, the sequencing of a genome is not without problems and pitfalls should
one not take proper precautions.
\emph{"The problem of sequence assembly can be compared to taking many copies
of a book, passing each of them through a shredder with a different cutter,
and piecing the text of the book back together just by looking at the shredded
pieces."}
Other than the high difficulty of the task, staying with the book analogy,
there are some additional problems that complicates this work significantly.
The original book could have several repeated paragraphs and some of the
shredded pieces may have been modified after the fact, essentially giving the
text typos that were not present before shredding. The container of the
shredder are not guaranteed to be emptied between each shredding resulting in
the possibility of pieces from another book being mixed in with the other
shreds or a shredded pieces may have been mangled beyond
recognition.\cite{seqmap}

There are two main branches when one should align sequences: de-novo assembly
and mapping assembly, one trying to match the pieces to each other while
the other relying on a reference to match the pieces to respectively.
De-novo assembly pieces each shred together, matching the ends to each other in
order to find the best fit, until the book is finished. By doing it this way
there is no way of knowing what kind of book we end up with, it being a
scientific book or a cartoon. Mapping assembly on the other hand searches the
entire reference to find the best suitable location for each shredded piece,
tossing away any shred that does not match the reference.

\subsection{Illumina}\label{DNAsm:Illumina}
The most commonly used sequencing technology today is provided by Illumina
\cite{illumina}.
The technology used is based on the two Cambridge scientists Shankar
Balasubramanian, Ph.D. and David Klenerman, Ph.D..
In the late 90's they formed the company Solexa and ultimately created the
Solexa sequencer in 2006, able to sequence 1 gigabase in one run.
Solexa were acquired in 2007 by Illumina which have taken the sequencer to new
hights. Their next generation sequencing data output have surpassed more than a
doubling each year and the current Illumina sequencer are able to generate more
than a terrabase of data each run.

\subsection{File format}\label{DNAsm:FileFormat}
\subsubsection*{Output}
When aligning reads during a DNA sequence mapping process, it is preferable that
the final product conforms to some standard in order for other programs, e.g.
tools that analyze the output, to extract the content.
Heng Li created a format called Sequence Alignment/Map (SAM) which is
\emph{"a generic format for storing large nucleotide sequence alignments"}
\cite{samgit}.
Aiming to be flexible enough to handle all of the alignment information created
by various alignment tools and simple enough to generate by these tools
or converted into from tools using other formats.
SAM also provide the possibility to allow most operations to be performed on the
alignment as a stream of data instead of having to load the whole alignment
into memory. It also allows the file to be indexed by genomic position to
retrieve all reads aligned to a locus while boasting a compact file size that
remains human readable \cite{samsource}.
There are two main components to SAM formated output. The header section and
the alignment section.\\
The header section of the output is not required, but must be located
at the beginning of the output file if present. This thesis will not provide
any header output and thusly the header information given in table
\ref{table:SAM_Head} is only for informational purposes.
The alignment section, on the other hand, is mandatory and contains information
about one or more alignments separated by a newline. Each individual alignment
information is tab separated just like the information in each header line. As
the alignment section contains 11 mandatory fields, as presented in table
\ref{table:SAM_Mandatory}. This thesis does not prioritize to provide all the
required information in the mandatory fields as the scope of the thesis is to
gage whether a Xeon Phi is suitable to perform alignments and there are far
more hardware strenuous activities that will weigh far heavier on the final
decision. Thusly, several of the default values will be utilized where possible
without loosing the alignment score and location itself. For information
regarding the individual fields see table \ref{table:SAM_Mandatory_Detailed}.
In addition to the 11 mandatory fields, there are several optional additions to
the alignment line if one wish to utilize them. This thesis takes advantage of
one of these additions in order to replace the MAPQ field with its default
value and use another score representation. The additional filed used in this
thesis is "AS:i:". Broken down, AS states that the following value is the
aligners own alignment score and i states that the value is an integer.
\begin{table}[!h]
    \begin{center}
        \begin{tabular}{|l|l|}
            \hline
            @HD & Begins the header line, if present \\
            \cline{2-2}
                &
            \begin{tabular}{lcl}
                VN* & - & Format version \\
                    &   & accepted format: / \^{} [0-9] + \ . [0-9] + \$ /\\
                SO  & - & Sorting order of alignments.\\
                    &   & Valid values: \emph{unknown}(default), \emph{unsorted},
                          \emph{queryname} and \emph{coordinate} \\
                GO  & - & Grouping of alignments. \\
                    &   & Indicating that similar alignments are grouped together \\
            \end{tabular} \\
            \hline
            @SQ & Reference sequence dictionary \\
            \cline{2-2}
                &
            \begin{tabular}{lcl}
                SN* & - & Reference sequence name \\
                    &   & Unique for each present @SQ line.\\
                LN* & - & Reference sequence length \\
                    &   & Range: [$1,2^{31}-1$]\\
                AS  & - & Genome assembly identifier \\
                M5  & - & MD5 checksum \\
                SP  & - & Species \\
                UR  & - & Sequence url \\
                    &   & eg. http: or ftp:. If not, file-system path assumed. \\
            \end{tabular} \\
            \hline
            @RG & Read group. Unordered multiple RG lines allowed. \\
            \cline{2-2}
                &
            \begin{tabular}{lcl}
                ID* & - & Read group identifier \\
                    &   & Unique for each present @RG line.\\
                CN  & - & Name of sequence center producing the read \\
                DS  & - & Description \\
                DT  & - & Date the run was produced \\
                FO  & - & Flow order \\
                KS  & - & Nucleotide array corresponding to key sequence of each read \\
                LB  & - & Library \\
                PG  & - & Program used for processing \\
                PI  & - & Predicted median insert size \\
                PL  & - & Platform/Technology used \\
                PM  & - & Platform model \\
                PU  & - & Platform unit \\
                SM  & - & Sample (Pool name for pool sequencing) \\
            \end{tabular} \\
            \hline
            @PG & Program\\
            \cline{2-2}
                &
            \begin{tabular}{lcl}
                ID* & - & Program record identifier \\
                    &   & Unique for each present @RG line.\\
                PN  & - & Program name \\
                CL  & - & Command line \\
                PP  & - & Previous @PG-ID \\
                DS  & - & Description \\
                VN  & - & Program version \\
            \end{tabular} \\
            \hline
            @CO & One-line text comment. Unordered multiple CO lines allowed.\\
            \hline
        \end{tabular}
    \end{center}
    \caption{SAM file format: header section}
    \label{table:SAM_Head}
\end{table}

\begin{table}[!h]
    \begin{center}
        \small{
        \begin{tabular}{|l|l|l|l|l|}
            \hline
            Col & Field & Type & Regexp/Range & Description \\
            \hline
            1 & QNAME & String & [!-?A-~],\{1,255\} & Query template name \\
            2 & FLAG & Int & $[0, 2^{16}-1]$ & Bitwise flag \\
            3 & RNAME & String & \*|[!-()+-<>-~][!-~]* & Reference sequence name \\
            4 & POS & Int & $[0, 2^{31}-1]$ & 1-based leftmost mapping position \\
            5 & MAPQ & Int & $[0, 2^{8}-1]$ & Mapping quality \\
            6 & CIGAR & String & \*|([0-9]+[MIDNSHPX=])+ & CIGAR string \\
            7 & RNEXT & String & \*|=|[!-()+-<>-~][!-~]* & Ref. name of next/mate read. \\
            8 & PNEXT & Int & $[0,2^{31}-1]$ & Position of next/mate read \\
            9 & TLEN & Int & $[-2^{31},2^{31}-1]$ & Observed template length \\
            10& SEQ & String & \*|[A-Za-z=.]+ & Segment sequence \\
            11& QUAL & String & [!-~]+ & ASCII of ASCII of Phred-scaled base\\
              &      &        &        & quality+33 \\
            \hline
        \end{tabular}
        }
    \caption{SAM file format: alignment section - overview}
    \label{table:SAM_Mandatory}
    \end{center}
\end{table}

\begin{table}[!h]
    \begin{center}
        \small{
        \begin{tabular}{|l|l|}
            \hline
            QNAME & Identical names are regarded as originating from the same template \\
            \hline
            FLAG & 
            \begin{tabular}{rcl}
                0x1 & - & template have multiple segments in sequencing\\
                0x2 & - & all segments properly aligned (according to aligner)\\
                0x4 & - & segment unmapped\\
                0x80 & - & next segment unmapped\\
                0x10 & - & SEQ being reverse-complimented\\
                0x20 & - & next SEQ being reverse-complimented\\
                0x40 & - & first segment in template\\
                0x80 & - & last segment in template\\
                0x100 & - & secondary alignment\\
                0x200 & - & not passing quality controls \\
                0x400 & - & PCR or optical duplicate\\
                0x800 & - & supplementary alignment\\
                \hline
                FLAG \& 0x900 = 0 & - & primary line of read\\
                      &   & only one read per SAM file should satisfy this
            \end{tabular}\\
            \hline
            RNAME & Reference sequence NAME of the alignment. For unmapped segments, set to * \\
            \hline
            POS & The first base in a reference sequence has coordinate 1. Set as 0 for an unmapped read. \\
                & If POS is 0, no assumptions can be made about RNAME and CIGAR.\\
            \hline
            MAPQ & Mapping Quality, equals 10log$_{10}$Pr\{mapping position is wrong\},\\
                 & rounded to the nearest integer.\\
                 & A value 255 indicates that the mapping quality is not available.\\
            \hline
            CIGAR & Mapping string of the same length as the Query.\\
                  & 
            \begin{tabular}{|c|l|}
                \hline
                Op & Description \\
                \hline
                M & Alignment match (match or mismatch) \\
                I & Insertion to the reference \\
                D & Deletion from the reference \\
                N & Skipped region from the reference \\
                S & Soft clipping (clipped sequences present in SEQ) \\
                  & May only have H between itself and closest end of CIGAR string \\
                H & Hard clipping (clipped sequences not present in SEQ) \\
                  & May only be at either end of the CIGAR string \\
                P & padding (silent deletion from padded reference) \\
                = & sequence match \\
                X & sequence mismatch \\
                \hline
            \end{tabular} \\
            &\\
            \hline
            RNEXT & RNAME of the next read in template. \\
                  & Set to * when information is unavailable \\
            \hline
            PNEXT & POS of the next read in template. \\
                  & Set to 0 when information is unavailable \\
            \hline
            TLEN & signed observed Template length.\\
                 & Set as 0 for single-segment template or when the information is unavailable.\\
            \hline
            SEQ & Segment sequence, may be * is segment is not stored.\\
            \hline
            QUAL & ASCII representation of MAPQ, set to * if unavailable.\\
            \hline
        \end{tabular}
        }
    \caption{SAM file format: alignment section - detailed}
    \label{table:SAM_Mandatory_Detailed}
    \end{center}
\end{table}



There is a trade off due to the human readability of the SAM file, it being more
strenuous to work with compared to BAM files, the binary equivalent to SAM.
SAMtools is an open source tool made to make life easier for humans and
computers alike, also created by Heng Li and freely downloadable
from github\cite{samgit}. It allows a user to freely convert SAM to BAM or
vice versa.

\subsubsection*{Sequence Input}
FASTQ\cite{fastq} \textit{"has recently become the \emph{de facto} standard for
storing the output of high throughput sequencing instruments such as the
Illumina Genome Analyzer."}
The FASTQ format, originally intended for to be a bundle for the preexisting
FASTA format, usually contain four line of information for each sequence:
\begin{itemize}
    \item{the sequence identifier, preceded by a '@' character.}
    \item{the raw sequence letters}
    \item{the third line is preceded by a '+' character and is optionally
        followed by the same sequence identifier and any additional description}
    \item{the last line contains the quality value for the sequence, in line
        two, and must have the same amount of characters as the sequence}
\end{itemize}
\noindent
The quality value characters, sorted with increasing quality:
\begin{verbatim}
!"#$%&'()*+,-./0123456789:;<=>?@
ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`
abcdefghijklmnopqrstuvwxyz{|}~
\end{verbatim}

\noindent
An example FASTQ file containing one sequence with the Illumina sequence identifiers
is presented in figure \ref{figure:FASTQ_example}.
\begin{figure}[!h]
    \begin{center}
        \begin{framed}
        \caption{FASTQ with Illumina sequence identifier}
        \label{figure:FASTQ_example}
\begin{verbatim}

@HWUSI-EAS100R:6:73:941:1973#0/1
GATTTGGGGTTCAAAGCAGTATCGATCAAATAGTAAATCCATTTGTTCAACTCACAGTTT
+
!''*((((***+))%%%++)(%%%%).1***-+*''))**55CCF>>>>>>CCCCCCC65

\end{verbatim}
The Illumina sequence identifier breakdown:
\begin{itemize}
    \item HWUSI-EAS100R - the unique instrument name
    \item 6 - flowcell lane
    \item 73 - tile number within the flowcell lane
    \item 941 - 'x'-coordinate of the cluster within the tile
    \item 1973 - 'y'-coordinate of the cluster within the tile
    \item \#0 - index number for a multiplexed sample (0 for no indexing)
    \item /1 - the member of a pair, /1 or /2 (paired-end or mate-pair reads only)
\end{itemize}
\end{framed}
\end{center}
\end{figure}

\subsubsection*{Genome Input}
Even though the FASTQ format would be suited to describe a reference genome,
the extra information provided by the two last lines is unnecessary as the
genome does not require the extra information to be useful.
Thusly the FASTA format, which FASTQ extends, that retains the two first lines of an
entry without the optional second retail of the sequence identifier or the
that it extends is suitable for describing a reference genome.\\
In addition, FASTA is not as strictly line bound as its extender and is divided
into two parts; The first line, preceeded with the greater than character '>',
describing what the entry contains, such as the Chromosome name and number,
and the rest of the entry containing the sequence itself over multiple lines if
required.
\begin{verbatim}
>gi|31563518|ref|NP_852610.1| microtubule-associated proteins 1A/1B light.....
MKMRFFSSPCGKAAVDPADRCKEVQQIRDQHPSKIPVIIERYKGEKQLPVLDKTKFLVPDHVNMSELVKI
IRRRLQLNPTQAFFLLVNQHSMVSVSTPIADIYEQEKDEDGFLYMVYASQETFGFIRENE
\end{verbatim}
