#ifndef READ_REF
#define READ_REF

#include "common.h"
#include "bwt.h"
#define READ_LENGTH 1024
#define INITIAL_SIZE 4*1024*1024*1024

void read_ref(char **ref, char *name);

#endif
