#ifndef SWSIMD
#define SWSIMD

# ifndef MIC_COMPILED

#include "emmintrin.h"
#include "smmintrin.h"

#define SW_MULTIPLE 4

#define __mxxxi __m128i
#define _simd_add_epi32 _mm_add_epi32
#define _simd_max_epi32 _mm_max_epi32
#define _simd_sub_epi32 _mm_sub_epi32

# else

#include "immintrin.h"
#include "zmmintrin.h"

#define SW_MULTIPLE 16

#define __mxxxi __m512i
#define _simd_add_epi32 _mm512_add_epi32
#define _simd_max_epi32 _mm512_max_epi32
#define _simd_sub_epi32 _mm512_sub_epi32

# endif

void retrieve_ref_info(storage_t *tmp, int q_len);
void sw_simd(__mxxxi *HE, storage_t *sp, char **q, int q_len, int multiple, int si, char *ref);
int create_partial_refs(char ***dst, storage_t *sp, int stored);
#endif
