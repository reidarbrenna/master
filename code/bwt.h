#ifndef BWT
#define BWT

#include "common.h"
#include "read_ref.h"

char *ref;              // Temporary pointer to the ref sequence.
char *L;                // Temporary pointer to hold L;
unsigned int *SAI;      // To hold every orig value until compression stage.
unsigned int ref_size; // To hold every orig value until compression stage.
unsigned int *location; // Index overview over where in ref a ned chromosone starts.


int compare(const void *pt1, const void *pt2);

void sort_SAI();
#endif
