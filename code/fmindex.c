#include "common.h"

/* Round_up(a/b); */
unsigned int iceil(unsigned int a, unsigned int b) {
    if (a%b)
        return (unsigned int)(a/b) + 1;
    return (unsigned int)a/b;
}

char *compress_L(char *dst, char *src, unsigned int length) {
    unsigned int i,j,c,o;
    char *bm = (char *)calloc(iceil(length + 1, 8),    sizeof(char));

    char tmp = 0x00;
    j = c = o = 0;
    fmi->orig[o++] = fmi->orig[j];
    for (i = 0; i <= length; i++) {
        // Compressing orig
        if (!(i%rov))
            fmi->orig[o++] = fmi->orig[i];

        if (src[i] == 'C')
            tmp |= 1;
        else if (src[i] == 'G')
            tmp |= 2;
        else if (src[i] == 'T')
            tmp |= 3;
        else if (src[i] == '$')
            fmi->idx[5][1] = i;
        else if (src[i] != 'A')
            bm[i/8] |= (0x1 << (7 - (i%8)));

        // As A has code 0 we do not need to add this to tmp

        if (++j == comp) {
            dst[c++] = tmp;
            tmp = 0x00;
            j = 0;
        } else {
            tmp = tmp << 2;
        }
    }
    //Padding rest of last tmp and adding it to out output:
    if (j != 0) {
        tmp = tmp << (comp - 1 - j) * 2;
        dst[c] = tmp;
    }
    return bm;
}

char *compress_ref(char *dst, char *src, unsigned int length) {
    unsigned int i,j,c;
    char *bm = (char *)calloc(iceil(length + 1, 8),    sizeof(char));

    char tmp = 0x00;
    j = c = 0;
    for (i = 0; i <= length; i++) {
        if (src[i] == 'C')
            tmp |= 1;
        else if (src[i] == 'G')
            tmp |= 2;
        else if (src[i] == 'T')
            tmp |= 3;
        else if (src[i] != 'A')
            bm[i/8] |= (0x1 << (7 - (i%8)));

        // As A has code 0 we do not need to add this to tmp

        if (++j == comp) {
            dst[c++] = tmp;
            tmp = 0x00;
            j = 0;
        } else {
            tmp = tmp << 2;
        }
    }
    //Padding rest of last tmp and adding it to out output:
    if (j != 0) {
        tmp = tmp << (comp - 1 - j) * 2;
        dst[c] = tmp;
    }
    return bm;
}

void extract(char *output, char c, char n) {
    int i;
    for (i = 0; i < sizeof(char)*4; i++) {
        if ((n & 0x1) == 0x1)
            output[(sizeof(char)*4) - 1 - i] = 'N';
        else if ((c & 0x3) == 0x0)
            output[(sizeof(char)*4) - 1 - i] = 'A';
        else if ((c & 0x3) == 0x1)
            output[(sizeof(char)*4) - 1 - i] = 'C';
        else if ((c & 0x3) == 0x2)
            output[(sizeof(char)*4) - 1 - i] = 'G';
        else
            output[(sizeof(char)*4) - 1 - i] = 'T';
        c = c >> 2;
        n = n >> 1;
    }
}

void init_index(unsigned int length, unsigned int location_length) {
    fmi = (fmindex *)malloc(sizeof(fmindex));
    fmi->ref =     (char *)malloc(iceil(length+1,comp)  * sizeof(char));
    fmi->refb =    (char *)malloc(iceil(length+1,8)     * sizeof(char));
    fmi->L =       (char *)malloc(iceil(length+1,comp)  * sizeof(char));
    fmi->Lb =      (char *)malloc(iceil(length+1,8)     * sizeof(char));
    fmi->orig =    (unsigned int *)malloc(iceil(length+1,rov)*6 * sizeof(unsigned int));
    fmi->rank =   (unsigned int **)malloc(5                     * sizeof(unsigned int *));
    fmi->rank[0] = (unsigned int *)malloc(iceil(length+1,rov)   * sizeof(unsigned int));
    fmi->rank[1] = (unsigned int *)malloc(iceil(length+1,rov)   * sizeof(unsigned int));
    fmi->rank[2] = (unsigned int *)malloc(iceil(length+1,rov)   * sizeof(unsigned int));
    fmi->rank[3] = (unsigned int *)malloc(iceil(length+1,rov)   * sizeof(unsigned int));
    fmi->rank[4] = (unsigned int *)malloc(iceil(length+1,rov)   * sizeof(unsigned int));
    fmi->idx =    (unsigned int **)malloc(6                     * sizeof(unsigned int *));
    fmi->idx[0] =  (unsigned int *)malloc(2                     * sizeof(unsigned int));
    fmi->idx[1] =  (unsigned int *)malloc(2                     * sizeof(unsigned int));
    fmi->idx[2] =  (unsigned int *)malloc(2                     * sizeof(unsigned int));
    fmi->idx[3] =  (unsigned int *)malloc(2                     * sizeof(unsigned int));
    fmi->idx[4] =  (unsigned int *)malloc(2                     * sizeof(unsigned int));
    fmi->idx[5] =  (unsigned int *)malloc(2                     * sizeof(unsigned int));
    fmi->location =(unsigned int *)malloc((location_length+1)   * sizeof(unsigned int));
}

/* Fill the global instanse of fmindex with content found in file */
void fill_index_file(FILE *file) {
    unsigned int length, loc_length, dump;
    dump = fread((void *)&length,      sizeof(unsigned int), 1,                  file);
    dump = fread((void *)&loc_length,  sizeof(unsigned int), 1,                  file);
    init_index(length, loc_length);
    dump = fread((void *)fmi->ref,     sizeof(char), iceil(length,comp), file);
    dump = fread((void *)fmi->refb,    sizeof(char), iceil(length,8),    file);
    dump = fread((void *)fmi->L,       sizeof(char), iceil(length,comp), file);
    dump = fread((void *)fmi->Lb,      sizeof(char), iceil(length,8),    file);
    dump = fread((void *)fmi->orig,    sizeof(unsigned int), iceil(length,rov),  file);
    dump = fread((void *)fmi->rank[0], sizeof(unsigned int), iceil(length,rov),  file);
    dump = fread((void *)fmi->rank[1], sizeof(unsigned int), iceil(length,rov),  file);
    dump = fread((void *)fmi->rank[2], sizeof(unsigned int), iceil(length,rov),  file);
    dump = fread((void *)fmi->rank[3], sizeof(unsigned int), iceil(length,rov),  file);
    dump = fread((void *)fmi->rank[4], sizeof(unsigned int), iceil(length,rov),  file);
    dump = fread((void *)fmi->idx[0],  sizeof(unsigned int), 2,                  file);
    dump = fread((void *)fmi->idx[1],  sizeof(unsigned int), 2,                  file);
    dump = fread((void *)fmi->idx[2],  sizeof(unsigned int), 2,                  file);
    dump = fread((void *)fmi->idx[3],  sizeof(unsigned int), 2,                  file);
    dump = fread((void *)fmi->idx[4],  sizeof(unsigned int), 2,                  file);
    dump = fread((void *)fmi->idx[5],  sizeof(unsigned int), 2,                  file);
    dump = fread((void *)fmi->location,sizeof(unsigned int),loc_length+1,        file);
    dump = dump*2;
    fclose(file);
}

/* Write index to file */
void index_to_file(FILE *file) {
    fwrite((void *)&(fmi->idx[5][0]), sizeof(unsigned int), 1,                      file);
    fwrite((void *)&(fmi->location[0]),sizeof(unsigned int), 1,                     file);
    fwrite((void *)(fmi->ref),     sizeof(char), iceil(fmi->idx[5][0],comp),file);
    fwrite((void *)(fmi->refb),    sizeof(char), iceil(fmi->idx[5][0],8),   file);
    fwrite((void *)(fmi->L),       sizeof(char), iceil(fmi->idx[5][0],comp),file);
    fwrite((void *)(fmi->Lb),      sizeof(char), iceil(fmi->idx[5][0],8),   file);
    fwrite((void *)(fmi->orig),    sizeof(unsigned int), iceil(fmi->idx[5][0],rov), file);
    fwrite((void *)(fmi->rank[0]), sizeof(unsigned int), iceil(fmi->idx[5][0],rov), file);
    fwrite((void *)(fmi->rank[1]), sizeof(unsigned int), iceil(fmi->idx[5][0],rov), file);
    fwrite((void *)(fmi->rank[2]), sizeof(unsigned int), iceil(fmi->idx[5][0],rov), file);
    fwrite((void *)(fmi->rank[3]), sizeof(unsigned int), iceil(fmi->idx[5][0],rov), file);
    fwrite((void *)(fmi->rank[4]), sizeof(unsigned int), iceil(fmi->idx[5][0],rov), file);
    fwrite((void *)(fmi->idx[0]),  sizeof(unsigned int), 2,                         file);
    fwrite((void *)(fmi->idx[1]),  sizeof(unsigned int), 2,                         file);
    fwrite((void *)(fmi->idx[2]),  sizeof(unsigned int), 2,                         file);
    fwrite((void *)(fmi->idx[3]),  sizeof(unsigned int), 2,                         file);
    fwrite((void *)(fmi->idx[4]),  sizeof(unsigned int), 2,                         file);
    fwrite((void *)(fmi->idx[5]),  sizeof(unsigned int), 2,                         file);
    fwrite((void *)(fmi->location),sizeof(unsigned int), fmi->location[0]+1,        file);
    fclose(file);
}

/* Fill the global instanse of fmindex with content found in char* */
void fill_index_char(unsigned int length, char *ref, char *L, unsigned int *location, unsigned int *SAI) {
    unsigned int i,j,c = 0;
    unsigned int A, C, G, T, N;
    char tmp;

    // Allocating space for fmi-index struct and setting preliminary ref, L & location:
    fmi = (fmindex *)malloc(sizeof(fmindex));
    fmi->ref = ref;
    fmi->L = L;
    fmi->location = location;
    fmi->orig = SAI;


    // Allocating space for the rank All pointers and F indexes.
    fmi->rank =   (unsigned int **)malloc(5*sizeof(unsigned int*));
    fmi->rank[0] = (unsigned int *)calloc(iceil(length + 1, rov), sizeof(unsigned int));
    fmi->rank[1] = (unsigned int *)calloc(iceil(length + 1, rov), sizeof(unsigned int));
    fmi->rank[2] = (unsigned int *)calloc(iceil(length + 1, rov), sizeof(unsigned int));
    fmi->rank[3] = (unsigned int *)calloc(iceil(length + 1, rov), sizeof(unsigned int));
    fmi->rank[4] = (unsigned int *)calloc(iceil(length + 1, rov), sizeof(unsigned int));
    fmi->idx =    (unsigned int **)malloc(6*sizeof(unsigned int*));
    fmi->idx[0] =  (unsigned int *)calloc(2,                      sizeof(unsigned int));
    fmi->idx[1] =  (unsigned int *)calloc(2,                      sizeof(unsigned int));
    fmi->idx[2] =  (unsigned int *)calloc(2,                      sizeof(unsigned int));
    fmi->idx[3] =  (unsigned int *)calloc(2,                      sizeof(unsigned int));
    fmi->idx[4] =  (unsigned int *)calloc(2,                      sizeof(unsigned int));
    fmi->idx[5] =  (unsigned int *)calloc(2,                      sizeof(unsigned int));


    /*
    printf("Creating compressed L, Lb and O column.\n");
    gettimeofday(&start,NULL);

    // Calculating the special case: starting @ index 0 always has '$' at the end.
    for (j = 0; j <= length; j++) {
        if (strcmp(ref,&(ref[j])) < 0)
            c++;
    }
        // No need to check preeceding char, we know it's an $ and will be
    // noted in fmi->eol.
    fmi->idx[5][1] = length - c;
    // If this turns out to be somewhere we should have stored original
    // position, we just did, as this row has the original position 0.

    // Sorting the rest of the rows and compressing the information.
#ifndef MAC_COMPILED
#pragma omp parallel for private(c,j) schedule(dynamic)
#endif
    for (i = 1; i <= length; i++) {
        c = 0;
        for (j = 0; j <= length; j++) {
            if (strcmp(&(ref[i]), &(ref[j])) < 0)
                c++;
        }
        if (ref[i-1] == 'C') {
            fmi->L[(length - c)/comp] |= (0x1 << (comp - 1 - ((length - c)%comp))*2);
        } else if (ref[i-1] == 'G') {
            fmi->L[(length - c)/comp] |= (0x2 << (comp - 1 - ((length - c)%comp))*2);
        } else if (ref[i-1] == 'T') {
            fmi->L[(length - c)/comp] |= (0x3 << (comp - 1 - ((length - c)%comp))*2);
        } else if (ref[i-1] != 'A') {
            fmi->Lb[(length - c)/8]   |= (0x1 << (7 - ((length - c)%8)));
        }

        if (!((length - c)%rov))
            fmi->orig[(length - c)/rov] = i;
    }
    */

    // We have now filled ref, L and orig as well as set eol and have thus no
    // more need for ref freeing to preserve space.

    //gettimeofday(&stop,NULL);
    //printf("Creation of compressed L, Lb & O: %fs\n",get_time_diff(start,stop));


    printf("Compressing ref, refb, L, Lb & Orig and setting eol\n");
    gettimeofday(&start,NULL);
    // Compressing ref to free up memory as well as allocating and filling ref
    // bitmap.
    fmi->refb = compress_ref(fmi->ref, fmi->ref, length);
    fmi->ref = (char *)realloc(fmi->ref, iceil(length+1, comp)*sizeof(char));
    fmi->Lb = compress_L(fmi->L, fmi->L, length);
    fmi->L = (char *)realloc(fmi->L, iceil(length+1, comp)*sizeof(char));
    fmi->orig = (unsigned int*)realloc(fmi->orig, iceil(length + 1, rov)*sizeof(unsigned int));

    gettimeofday(&stop,NULL);
    printf("Compression done in %fs\n",get_time_diff(start,stop));


    printf("Creating compressed rank collumns for A, C, G, T & N\n");
    gettimeofday(&start,NULL);

    // Building rankAll columns.
    char test[comp];
    j = c = 0;
    A = C = G = T = N = 0;

    extract(test, fmi->L[0], (char)(fmi->Lb[0] >> 4));
    if (!fmi->idx[5][1]) //eol
        test[0] = '$';
    if (test[0] == 'A') {
        fmi->rank[0][0] = ++A;
    } else if (test[0] == 'C') {
        fmi->rank[1][0] = ++C;
    } else if (test[0] == 'G') {
        fmi->rank[2][0] = ++G;
    } else if (test[0] == 'T') {
        fmi->rank[3][0] = ++T;
    } else if (test[0] == 'N') {
        fmi->rank[4][0] = ++N;
    }

    tmp = 0x1;
    for (i = 1; i <= length; i++) {
        if (!(i%comp)) {
            if (tmp) {
                extract(test, fmi->L[i/comp],fmi->Lb[i/8]);
                tmp = 0x0;
            } else {
                extract(test, fmi->L[i/comp],(char)(fmi->Lb[i/8] >> 4));
                tmp = 0x1;
            }
        }
        if (i == fmi->idx[5][1]) //eol
            test[i%comp] = '$';
        if (!(i%rov)) {
            if (test[i%comp] == 'A') {
                fmi->rank[0][i/rov] = ++A;
                fmi->rank[1][i/rov] = C;
                fmi->rank[2][i/rov] = G;
                fmi->rank[3][i/rov] = T;
                fmi->rank[4][i/rov] = N;
            } else if (test[i%comp] == 'C') {
                fmi->rank[0][i/rov] = A;
                fmi->rank[1][i/rov] = ++C;
                fmi->rank[2][i/rov] = G;
                fmi->rank[3][i/rov] = T;
                fmi->rank[4][i/rov] = N;
            } else if (test[i%comp] == 'G') {
                fmi->rank[0][i/rov] = A;
                fmi->rank[1][i/rov] = C;
                fmi->rank[2][i/rov] = ++G;
                fmi->rank[3][i/rov] = T;
                fmi->rank[4][i/rov] = N;
            } else if (test[i%comp] == 'T') {
                fmi->rank[0][i/rov] = A;
                fmi->rank[1][i/rov] = C;
                fmi->rank[2][i/rov] = G;
                fmi->rank[3][i/rov] = ++T;
                fmi->rank[4][i/rov] = N;
            } else if (test[i%comp] == 'N') {
                fmi->rank[0][i/rov] = A;
                fmi->rank[1][i/rov] = C;
                fmi->rank[2][i/rov] = G;
                fmi->rank[3][i/rov] = T;
                fmi->rank[4][i/rov] = ++N;
            } else {
                fmi->rank[0][i/rov] = A;
                fmi->rank[1][i/rov] = C;
                fmi->rank[2][i/rov] = G;
                fmi->rank[3][i/rov] = T;
                fmi->rank[4][i/rov] = N;
            }
        } else {
            if (test[i%comp] == 'A') {
                A++;
            } else if (test[i%comp] == 'C') {
                C++;
            } else if (test[i%comp] == 'G') {
                G++;
            } else if (test[i%comp] == 'T') {
                T++;
            } else if (test[i%comp] == 'N') {
                N++;
            }
        }
        j++;
    }

    // Filling char index ref guide.
    fmi->idx[0][0] = 1;                     // Start A.
    fmi->idx[0][1] = A;                     // End A.
    fmi->idx[1][0] = 1 + A;                 // Start C.
    fmi->idx[1][1] = A + C;                 // End C.
    fmi->idx[2][0] = 1 + A + C;             // Start G.
    fmi->idx[2][1] = A + C + G;             // End G.
    fmi->idx[3][0] = 1 + A + C + G + N;     // Start T.
    fmi->idx[3][1] = A + C + G + N + T;     // End T.
    fmi->idx[4][0] = 1 + A + C + G;         // Start N.
    fmi->idx[4][1] = A + C + G + N;         // End N.
    fmi->idx[5][0] = 1 + A + C + G + N + T; // Length.

    gettimeofday(&stop,NULL);
    printf("Creation of compressed rank collumns: %fs\n",get_time_diff(start,stop));
}

/* Free all internal pointers of global fmindex */
void kill_index() {
    if (fmi->L != NULL) {
        free(fmi->ref);
        free(fmi->refb);
        free(fmi->L);
        free(fmi->Lb);
        free(fmi->orig);
        free(fmi->rank[0]);
        free(fmi->rank[1]);
        free(fmi->rank[2]);
        free(fmi->rank[3]);
        free(fmi->rank[4]);
        free(fmi->rank);
        free(fmi->idx[0]);
        free(fmi->idx[1]);
        free(fmi->idx[2]);
        free(fmi->idx[3]);
        free(fmi->idx[4]);
        free(fmi->idx[5]);
        free(fmi->idx);
        free(fmi->location);
        free(fmi);
    }
}

/* Print content to screen, Debug/Curiosity purposes only! */
void print_index() {
    unsigned int i;
    char flag = 0x0;
    char tmp[comp];

    printf("\n_____CONTENT_OF_EXTENDED_FM-INDEX_____\n\n");
    printf("Chromosone location in ref genome:\n");
    for (i = 1; i <= fmi->location[0]; i++) {
        printf("\tFile #%u located @ index: %u\n",i,fmi->location[i]);
    }

    printf("\nReference sequence:");
    for (i = 0; i < fmi->idx[5][0] - 1; i++) {
        if (!(i%80))
            printf("\n");
        if (!(i%comp)) {
            if (flag) {
                extract(tmp, fmi->ref[i/comp], fmi->refb[i/8]);
                flag = 0x0;
            } else {
                extract(tmp, fmi->ref[i/comp], (char)(fmi->refb[i/8] >> 4));
                flag = 0x1;
            }
        }
        printf("%c",tmp[i%comp]);
    }
    printf("$\n");

    printf("\n  I  | L |  O  |  A  |  C  |  G  |  T  |  N\n");
    printf("---------------------------------------------\n");
    flag = 0x0;
    for (i = 0; i < fmi->idx[5][0]; i++) {
        // Time to get new set of chars from l?
        if (!(i%comp)) {
            if (flag) {
                extract(tmp, fmi->L[i/comp], fmi->Lb[i/8]);
                flag = 0x0;
            } else {
                extract(tmp, fmi->L[i/comp], (char)(fmi->Lb[i/8] >> 4));
                flag = 0x1;
            }
        }

        if (i == fmi->idx[5][1])
            tmp[i%comp] = '$';
        // Time to print rank and orig aswell?
        if (!(i%rov)) {
            printf(" %3u | %c | %3u | %3u | %3u | %3u | %3u | %3u\n",i,tmp[i%comp]
                                                                      ,fmi->orig[i/rov]
                                                                      ,fmi->rank[0][i/rov]
                                                                      ,fmi->rank[1][i/rov]
                                                                      ,fmi->rank[2][i/rov]
                                                                      ,fmi->rank[3][i/rov]
                                                                      ,fmi->rank[4][i/rov]);
        } else {
            printf(" %3u | %c |  -  |  -  |  -  |  -  |  -  |  -\n",i,tmp[i%comp]);
        }
    }
    printf("---------------------------------------------\n\n");
    printf("A(00|0) found in range[%u,%u]\n",fmi->idx[0][0], fmi->idx[0][1]);
    printf("C(01|0) found in range[%u,%u]\n",fmi->idx[1][0], fmi->idx[1][1]);
    printf("G(10|0) found in range[%u,%u]\n",fmi->idx[2][0], fmi->idx[2][1]);
    printf("T(11|0) found in range[%u,%u]\n",fmi->idx[3][0], fmi->idx[3][1]);
    printf("N(00|1) found in range[%u,%u]\n",fmi->idx[4][0], fmi->idx[4][1]);
    printf("\nLength of reference sequence: %u\n",fmi->idx[5][0]);
    printf("\nL eol @ index: %u\n",fmi->idx[5][1]);
}
