#ifndef FMI_IO
#define FMI_IO

//#include "search.h"
#include "common.h"
#include "storage.h"

#define SEQ_ID_LEN   20
#define Q_LEN       200
#define INPUT_LEN  2000

//INPUT:
char *read_dst;

int read_input(FILE *fp, char *QNAME, char *Q);

//OUTPUT
int matches_to_return;

typedef struct sam_content {
    //FILE *outfile;
    char RNAME[3];
    char *QNAME;
    unsigned int POS;
    int MAPQ;
    char *CIGAR;
    char *SEQ;
} sam_t;

void write_to_file(FILE *fp, sam_t *sam, storage_t *match, int len);
#endif
