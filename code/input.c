#include "io.h"

int read_input(FILE *fp, char *QNAME, char *Q) {
    if (fp == NULL)
        return 1;
    while (fgets(read_dst, INPUT_LEN, fp) != NULL) {
        if (read_dst[0] == '@') {
            strncpy(QNAME, &(read_dst[1]), SEQ_ID_LEN);
            QNAME[SEQ_ID_LEN] = '\0';
            break;
        }
    }
    if (fgets(read_dst, INPUT_LEN, fp) == NULL)
        return 1;
    strncpy(Q,read_dst,Q_LEN);

    // skipping additional information Line.
    fgets(read_dst, INPUT_LEN, fp);
    //XXX: skipping query quality for now!
    fgets(read_dst, INPUT_LEN, fp);
    return 0;
}
