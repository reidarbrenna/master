#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <offload.h>

#define ALLOC alloc_if(1)
#define FREE free_if(1)
#define RETAIN free_if(0)
#define REUSE alloc_if(0)

int main(int argc, char *argv[]) {
    int *data = _mm_malloc(100*sizeof(int),64);
    int i;
#pragma offload_transfer target(mic:0) nocopy(data : length(0) ALLOC RETAIN)
{}

    for (i=0; i < 100; i++)
        data[i] = i;

#pragma offload target(mic:0) in(data : length(100) REUSE RETAIN) signal("sig1")
    {
#pragma vector aligned
#pragma omp parallel for
    for (i=0; i < 100; i++)
        printf("%d ",data[i]);
    printf("\n");
    }

printf("TEST ");

#pragma offload target(mic:0) wait("sig1") nocopy(data : length(0) FREE)

printf("last?\n");
    _mm_free(data);
    return 0;
}
