#include "common.h"
#include "read_ref.h"
#include "bwt.h"

void print_help() {
        printf("USAGE:\n");
        printf("./fmi_build [-p][-h] <output_file> <files containing the reference sequence>\n");
        printf("\t - \"-p\" turns on print of extended fm-index at the end. \n");
        printf("\t - \"-h\" prints this msg and terminates the execution\n");
        printf("\t - An output file is needed to store the compressed fmi-index in. \n");
        printf("\t - All subsequent filenames after output will be traversed in in order\n");
        printf("\t   to create the fm-index\n");
        printf("\t   Note: The files are asumed to be in this order; chromosone 1-22,MT,X,Y,uncertain\n");
}

int main(int argc, char *argv[]) {
    unsigned int i, offset = 1, num_threads = 1;
    char print_flag = 0, from_file_flag = 0;

    if (argc < 3) {
        print_help();
        return 1;
    }

    // Initializing global ints:
    ref_size = 0;

    while (1) {
        if (!strcmp(argv[offset], "-p")) {
            print_flag = 1;
        } else if (!strcmp(argv[offset], "-h")) {
            print_help();
            return 0;
        } else if ((!strcmp(argv[offset], "-n")) && (offset < argc)) {
            num_threads = atoi(argv[offset+1]);
            offset++;
        } else if ((!strcmp(argv[offset], "-fl")) && (offset < argc)) {
            from_file_flag = 1;
        } else {
            break;
        }
        offset++;
    }

#ifndef MAC_COMPILED
    if ((num_threads > 1) && (num_threads%2))
        num_threads--;
    printf("Setting number of threads to: %d\n",num_threads);

    omp_set_num_threads(num_threads);
# ifdef MIC_COMPILED
    kmp_set_defaults("KMP_AFFINITY=scatter");
# endif
#endif

    ref = (char *)malloc((long)INITIAL_SIZE*sizeof(char));

    offset++;
    gettimeofday(&init, NULL);
    if (!from_file_flag) {
        location = (unsigned int *)malloc((argc - offset + 1) * sizeof(unsigned int));
        location[0] = (unsigned int)(argc - offset);
        printf("Retrieving ref sequence from %u file(s):\n",location[0]);
        // Concat all ref information from input files:
        for (i = offset; i < argc; i++) {
            printf("\t%s\n",argv[i]);
            location[i - offset + 1] = ref_size;
            read_ref(&ref, argv[i]);
        }
    } else {
        char *filename = (char *)malloc(70*sizeof(char));
        unsigned int filename_length = 0;
        unsigned int number_of_files = 0;
        FILE *file = fopen(argv[offset], "r");
        fgets(filename,70,file);
        number_of_files = atoi(filename);
        printf("Retrieving ref sequence from %u files found in file list: %s\n",number_of_files,argv[offset]);
        location = (unsigned int *)malloc((number_of_files + 1) * sizeof(unsigned int));
        location[0] = (unsigned int)number_of_files;

        i = 1;
        while (fgets(filename,70,file)) {
            filename_length = strlen(filename);
            if (filename[filename_length - 1] == '\n')
                filename[filename_length - 1] = '\0';
            printf("%s\n",filename);
            location[i++] = ref_size;
            read_ref(&ref, filename);
        }
        fclose(file);
        free(filename);
    }
    ref_size--;
    ref[ref_size] = '$';
    ref =(char *)realloc(ref, (ref_size + 1)*sizeof(char));
    gettimeofday(&stop, NULL);
    offset--;


    if (!ref_size) {
        printf("No sequence data retrieved from input file(s)\n");
        free(ref);
        free(location);
        return 1;
    }
    printf("Retrieval of ref sequence: %fs\n",get_time_diff(init, stop));

    printf("Creating an L of size: %u\n",ref_size+1);
    gettimeofday(&init, NULL);
    sort_SAI(num_threads);
    gettimeofday(&stop,NULL);
    printf("Created L in %fs\n",get_time_diff(init, stop));

    printf("Creating extended fm-index.\n");
    gettimeofday(&init,NULL);
    fill_index_char(ref_size, ref, L, location, SAI);
    gettimeofday(&stop,NULL);
    printf("Creation of extended fm-index took: %fs\n",get_time_diff(init, stop));

    printf("Creating compressed reference sequence (.crs) file\n");
    gettimeofday(&init,NULL);
    i = strlen(argv[offset]);
    char *filename = (char *)calloc(i + 5,sizeof(char));
    strcpy(filename, argv[offset]);
    strcpy(&(filename[i]), ".crs");
    FILE *file = fopen(filename, "wb");
    index_to_file(file);
    gettimeofday(&stop,NULL);
    printf("Creation of .crs file: %fs\n",get_time_diff(init, stop));
    free(filename);

    if (print_flag)
        print_index();
    kill_index();
    return 0;
}
