#include "search.h"
#include "simd.h"
#include "simd_globals.h"

int create_partial_refs(char ***dst, storage_t *sp, int stored) {
    int i, j, n = iceil(stored,SW_MULTIPLE);
    int overflow = stored%SW_MULTIPLE;
    char c;

    *dst = (char **)malloc(n*sizeof(char*));
    for (i = 0; i < n; i++)
        (*dst)[i] = (char *)malloc(SW_MULTIPLE*(Q_LEN + 2*SW_REF_PADDING)*sizeof(char));
    for (i = 0; i < stored; i++) {
        retrieve_ref_info(&(sp[i]), Q_LEN);
        for (j = 0; j < Q_LEN + 2*SW_REF_PADDING; j++) {
            if ((fmi->refb[(sp[i].ref_index + j)/8] >> (7 - ((sp[i].ref_index + j)%8))) & 0x1)
                c = 'N';
            else
                c = ((fmi->ref[(sp[i].ref_index + j)/comp] >> ((comp - 1 - ((sp[i].ref_index + j)%comp))*2)) & 0x3);
            (*dst)[i/SW_MULTIPLE][j*SW_MULTIPLE+(i%SW_MULTIPLE)] = c;
        }
    }
    return overflow;
}

void retrieve_ref_info(storage_t *tmp, int q_len) {
    if (tmp == NULL)
        return;

    unsigned int j, index;
    unsigned int r_len = 2*SW_REF_PADDING + q_len;
    unsigned int ref_sequence = 0;
    unsigned int ref_index = 0;
    // Retrieving ref sequence number:
    for (j = 2; j <= fmi->location[0]; j++) {
        if (tmp->ref_index < fmi->location[j]) {
            ref_sequence = j - 1;
            break;
        }
    }
    if (!ref_sequence)
        ref_sequence = fmi->location[0];

    // Retrieving ref start index
    if (tmp->query_index + SW_REF_PADDING >= tmp->ref_index)
        ref_index = fmi->location[ref_sequence];
    else {
        index = tmp->ref_index - tmp->query_index - SW_REF_PADDING;
        if (ref_sequence == fmi->location[0]) {
            if (index < fmi->location[ref_sequence])
                ref_index = fmi->location[ref_sequence];
            else if (index + r_len >= fmi->idx[5][0])
                ref_index = fmi->idx[5][0] - (q_len + 2*SW_REF_PADDING);
            else
                ref_index = index;
        } else {
            if (index < fmi->location[ref_sequence])
                ref_index = fmi->location[ref_sequence];
            else if (index + r_len >= fmi->location[ref_sequence + 1])
                ref_index = fmi->location[ref_sequence + 1] - (q_len + 2*SW_REF_PADDING);
            else
                ref_index = index;
        }
    }
    tmp->ref_index = ref_index;
    tmp->query_index = ref_sequence;
}

void sw_simd(__mxxxi *HE, storage_t *sp, char **q, int q_len, int multiple, int si, char *ref) {
    int score_results[SW_MULTIPLE];
    char __attribute__((aligned(64))) o[multiple];
    unsigned int i, j, t;
    unsigned int r_len = q_len + (2*SW_REF_PADDING);
    //__mxxxi *HE = (__mxxxi *)malloc(2*q_len*sizeof(__mxxxi));

    __mxxxi H, E, F, N, SM, S;

    S = *(__mxxxi *)ZERO;

    memset(HE, 0, 2*q_len*sizeof(__mxxxi));
    for (i = 0; i < multiple; i++) {
        o[i] = sp[si+i].reversed_flag;
    }


    for (i = 0; i < r_len; i++) {
        H = *(__mxxxi*)ZERO;
        F = *(__mxxxi*)ZERO;

        for (j = 0; j < q_len; j++) {
            N = HE[j*2 + 0];
            E = HE[j*2 + 1];

            for (t = 0; t < multiple; t++)
                score_results[t] = score(q[o[t]*2], q[o[t]*2 + 1], j, ref[i*SW_MULTIPLE + t]);
            SM = *(__mxxxi*)score_results;

            H = _simd_add_epi32(H,SM);
            H = _simd_max_epi32(H,F);
            H = _simd_max_epi32(H,E);
            H = _simd_max_epi32(H,*(__mxxxi*)ZERO);
            S = _simd_max_epi32(H,S);

            HE[j*2 + 0] = H;

            E = _simd_sub_epi32(E,*(__mxxxi*)GE);
            F = _simd_sub_epi32(F,*(__mxxxi*)GE);
            H = _simd_sub_epi32(H,*(__mxxxi*)GOE);
            E = _simd_max_epi32(H,E);
            F = _simd_max_epi32(H,F);

            HE[j*2 + 1] = E;
            H = N;
        }
    }

    for (i = 0; i < multiple; i++) {
        sp[si+i].score = ((int *)&S)[i];
    }

    //free(HE);
}
