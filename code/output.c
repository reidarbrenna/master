#include "io.h"

void write_to_file(FILE *fp, sam_t *sam, storage_t *match, int len) {
    int FLAG = 0x0;
    int i,j = 1;
    char prev = (sam->CIGAR)[0];

    if (match->reversed_flag)
        FLAG |= 0x10;

    fprintf(fp, "%s\t%d\t%s\t%u\t%d\t",sam->QNAME,FLAG,sam->RNAME,sam->POS+1,sam->MAPQ);

    for (i = 1; i < 2*Q_LEN + 100; i++) {
        if ((sam->CIGAR)[i] != prev) {
            fprintf(fp,"%d%c",j,prev);
            prev = (sam->CIGAR)[i];
            j = 1;
        } else {
            ++j;
        }
        if ((sam->CIGAR)[i] == '\0')
            break;
    }
    if (j != 1)
        fprintf(fp,"%d%c",j,prev);
    fprintf(fp, "\t*\t0\t0\t%s\t*\tAS:i:%d\n",sam->SEQ,match->score);
}
