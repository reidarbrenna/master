#include "common.h"
#include "search.h"
#include "io.h"
#ifndef MAC_COMPILED
#include "simd.h"
#endif

void print_help() {
    printf("USAGE:\n");
    printf("./DNAlign [flags] <ref> [query]\n");
    printf("\t<ref>    the .crs file containing the fm_index.\n");
    printf("\tquery    all filenames given after <ref> are considered query files.\n");
    printf("\t\t    * all queries in the file(s) should be newline sepparated (however, a compleately blank line is not needed)\n");
    printf("\nFLAGS - all optional\n");
    printf("\t-h       prints this help message before terminating the execution\n");
    printf("\t-p       print content of fm_index to debug.out (not recomended!)\n");
    printf("\t-n <int> number of threads to use - default 1\n");
    printf("\t-m <int> number of best matches to return - default 1\n");
    printf("\t-l <int> minimum length of partial matches to keep - default 20\n");
    printf("\t\t    * Higher = longer perfect matches required when searching for partials!\n");
    printf("\t\t    * Lower  = shorter perfect matches required when searching for partials!\n");
}

int main(int argc, char *argv[]) {
    FILE *fp = NULL;
    FILE *ofp = NULL;

    char print_flag = 0x0;

    int offset = 1;

    matches_to_return = 1;
    num_threads = 1;
    length_limit = 20;

    while (1) {
        if ((offset == argc) || (!strcmp(argv[offset], "-h"))) {
            print_help();
            return 0;
        } else if ((offset > argc) || (!strcmp(argv[offset], "-p"))) {
            print_flag |= 0x1;
        } else if ((!strcmp(argv[offset], "-n")) && (offset < argc)) {
            num_threads = atoi(argv[++offset]);
        } else if ((!strcmp(argv[offset], "-m")) && (offset < argc))  {
            matches_to_return = atoi(argv[++offset]);
        } else if ((!strcmp(argv[offset], "-l")) && (offset < argc))  {
            length_limit = atoi(argv[++offset]);
        } else {
            break;
        }
        ++offset;
    }
    if (num_threads <= 2)
        num_threads = 1;

    omp_set_num_threads(num_threads);
#ifdef MIC_COMPILED
    kmp_set_defaults("KMP_AFFINITY=scatter");
#endif

    // part 0: read ref into memory.
    fp = fopen(argv[offset++],"rb");

    if (fp == NULL) {
        printf("Could not open reference file: %s\n",argv[1]);
        return 1;
    }

    fill_index_file(fp);

    printf("Index received!\n");
    if (print_flag)
        print_index();

    // part 1 preparation!
    fp = fopen(argv[offset++], "r");
    if (fp == NULL) {
        printf("ERROR: could not open query file!\n");
    }

    read_dst =        (char *)malloc(INPUT_LEN                           * sizeof(char));
    sam_t *content = (sam_t *)malloc(                                      sizeof(sam_t));
    content->QNAME =  (char *)malloc((SEQ_ID_LEN+1)                      * sizeof(char));
    content->SEQ   =  (char *)malloc((Q_LEN+1)                           * sizeof(char));
    content->CIGAR =  (char *)malloc((2*Q_LEN + 2*SW_REF_PADDING + 1)    * sizeof(char));
    content->MAPQ = 255;
    content->SEQ[Q_LEN] =        '\0';
    content->QNAME[SEQ_ID_LEN] = '\0';
    ofp = fopen("output.sam","w");


    while (1) {
        // part 1: read next query.
        if (read_input(fp,content->QNAME, content->SEQ)) {
            if (offset <= argc) {
                fclose(fp);
                fp = fopen(argv[offset++], "r");
                if (fp == NULL) {
                    break;
                }
            } else {
                break;
            }
        } else {
            // part 2-6 located inside.
            // XXX: Internal allocations could be moved out here.
            //printf("name:%s\nquery:%s\n",content->QNAME, content->SEQ);
            search(ofp, content, matches_to_return);
        }
    }

    // Clean up allocations.
    free(content->SEQ);
    free(content->CIGAR);
    free(content->QNAME);
    free(content);

    kill_index();
    return 0;
}
