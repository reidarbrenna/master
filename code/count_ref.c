#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    char *dst = (char *)calloc(500,sizeof(char));
    int tmp, length = 0, lines = 0;
    FILE *fp = fopen(argv[1],"r");

    while (fgets(dst,500,fp) != NULL) {
        if ((dst[0] != '>')&&(dst[0] != '@')) {
            lines++;
            tmp = strlen(dst);
            if (dst[tmp-1] == '\n')
                length += tmp-1;
            else
                length += tmp;
        }
    }
    printf("%s has %d chars (excluding headers) on %d lines\n",argv[1],length, lines);
    return 0;
}
