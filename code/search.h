#ifndef SEARCH
#define SEARCH

#include "common.h"
#include "storage.h"
#include "io.h"

#ifdef MIC_COMPILED
#define free _mm_free
#define malloc(x) _mm_malloc(x,64)
#endif

#define SW_REF_PADDING  5

#define MATCH           1
#define MISSMATCH       -2

#define GAP_OPEN        5
#define GAP_EXTEND      2
#define GAP_OPEN_EXTEND (GAP_OPEN + GAP_EXTEND)

int num_threads;
int length_limit;
//int num_thread_numbers;
/* search
 *
 * Search for a partial (or full) allignment of given query sequence Q in ref.
 * if full match is found, SW for that match will be ommited and a perfect
 * score will be given.
 *
 * Due to an extra N at the end of every sequence in ref a (partial) match will
 * never cross sequence borders.
 */
void search(FILE *fp, sam_t *sam, int m);

/* score
 *
 * returns the value of this scoring matrix based on input:
 *   A C G N T
 * A 1 0 0 0 0
 * C 0 1 0 0 0
 * G 0 0 1 0 0
 * N 0 0 0 0 0
 * T 0 0 0 0 1
 * where 1 represents MATCH and 0 MISSMATCH
 */
int score(char *q, char *bm, int q_index, char r);

/* full_SW
 *
 * Fills the whole H matrix and stores the i and j index of the highest match
 * in score_index[0] & [1] respectively before returning the score to the caller.
 */
int full_SW(storage_t *match, char *q, char *bm, int q_len, int r_len, int *H, int *E, unsigned int ref_index, int *score_index);
void create_CIGAR(sam_t *sam, int *H, int *score_index);
#endif
