#include "read_ref.h"

void read_ref(char **ref, char *name) {
    unsigned int read, length = ref_size;

    // Open file
    FILE *file = fopen(name, "r");

    // Succesfully?
    if (file == NULL)
        return;

    while (1) {
        // Do we need more space?
        //if ((ref_size - length) < READ_LENGTH) {
        //    *ref = (char *)realloc(*ref, (ref_size + READ_LENGTH)*sizeof(char));
        //    ref_size += READ_LENGTH;
        //}
        if (fgets(&((*ref)[length]),READ_LENGTH,file) == 0)
            break;
        read = strlen(&((*ref)[length]));

        if ((*ref)[length] != '>') {
            if ((*ref)[length+read-1] == '\n')
                read--;
            length += read;
        } else {
            printf("  - %s",&((*ref)[length+1]));
        }
    }

    // Closing file.
    fclose(file);

    // Adding un determined('N') at the end in order to prevent FL-search to
    // match across multiple chromosones.
    (*ref)[length++] = 'N';
    // Adding end of ref sign ('$')
    (*ref)[length] = '$';

    // Reallocating ref size: we don't want any more memory allocated than we
    // need to! (although, we add one more at the end in order to terminate the
    // string.)
    //*ref = (char *)realloc(*ref, (length + 1)*sizeof(char));
    ref_size = length;
}
