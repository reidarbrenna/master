#ifndef COMMON
#define COMMON

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <limits.h>
#include <sys/time.h>
#include <time.h>
# ifndef MAC_COMPILED
#include <omp.h>
# endif
#include "fmindex.h"

/* Definitions:
 *
 * Changing any of these definitions renders previous .crs files moot.
 * Recreate any .crs files you use after changing definitions.
 *
 * rov needs to have a value of comp*X, x = 1,2,...
 */
#define comp 4 // 2bytes pr char from ref combines to create one new..
//#define rov comp*1          // every rov.th entry in L get to keep their rank and orig value.
#define rov 64                 // every rov in L get to keep their rank and orig value.

// Globals:
fmindex *fmi;

struct timeval init, start, stop;
double get_time_diff(struct timeval t1, struct timeval t2);

#endif
