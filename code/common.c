#include "common.h"

double get_time_diff(struct timeval t1, struct timeval t2) {
    double s = (double)t1.tv_sec + (double)t1.tv_usec/1000000;
    double e = (double)t2.tv_sec + (double)t2.tv_usec/1000000;
    return e-s;
}
