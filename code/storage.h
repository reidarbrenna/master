#ifndef STORAGE
#define STORAGE

#include "common.h"

#define MAX_STORE 16*100       // How many best matches do we want to keep? XXX: MUST BE LARGER THAN 1
//#define MAX_STORE 16       // How many best matches do we want to keep? XXX: MUST BE LARGER THAN 1

typedef struct storage {
    char reversed_flag;
    int score;
    int length;
    unsigned int ref_index;
    unsigned int query_index;
    struct storage *next;
    struct storage *prev;
} storage_t;


/* add
 * Add a new storage struct to the list of stored. will allways update the
 * lowest pointer and returns a pointer to the highest stored value.
 * The list will be sorted according to length. longest to shortest.
 *
 * @param storage_space: pointer to a malloed area of size MAX_STORE*sizeof(storage_t)
 * @param high: pointer to the previously highest stored entry in storage_space.
 * @param low: Same as high, but the lowest instead. (Note: this is the address of the pointer and not the struct itself).
 * @param storage_idx: pointer to an int that should have been initialized to 0. (no need to worry about this int later).
 * @param length: the length of the new match.
 * @param ref_index: where the new match start in ref.
 * @param query_index: where the new match start in query.
 * @param rev_flag: if 0: new match calculated from original query.
 *                  else: new match calculated from reverse complimented query.
 *                  (i.e.: A->T, C->G, G->C, T->A, N->N and in reversed order)
 */
storage_t *add_new(storage_t *storage_space, storage_t *high, storage_t **low, int *storage_idx, int length, unsigned int ref_index, int query_index, char rev_flag, int ql);

void sort_storage_length(storage_t *storage, int stored);
void sort_storage_score(storage_t *storage, int stored);

/* DEBUG printout of storage content */
void print_storage(storage_t *root, char *o, char *bm_o, char *r, char *bm_r, int q_len);

#endif
