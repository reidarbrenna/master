#ifndef FMINDEX
#define FMINDEX

/* FM-INDEX content explanation:
 * The fmindex keeps track of several parts that make up the extended fm-index.
 * - ref and refb holds the compressed {A,C,G,T} and {N} of the genome respectively.
 * - L and Lb does the same as above but keep track of the BWT of the genome.
 * - orig maps each letter in L to its original location in ref. (i.e. where
 *   the corresponding letter in the BWT originated in the genome).
 *   Note that only every rov-th letter will retain this index (atm rov = 64).
 * - rank is an 2-dim array that tracks the how many times each of A, C, G, T and N
 *   have occured since the beggining of L.
 *    - rank[0][...] = A
 *    - rank[1][...] = C
 *    - rank[2][...] = G
 *    - rank[3][...] = T
 *    - rank[4][...] = N
 *    Note that only every rov-th row in the matrix will be kept. (atm rov = 64).
 * - idx hold the index range of each letter in the first collumn of the sorted
 *   BW-Matrix.
 *    - A ∈ [ idx[0][0], idx[0][1] ]
 *    - C ∈ [ idx[1][0], idx[1][1] ]
 *    - G ∈ [ idx[2][0], idx[2][1] ]
 *    - T ∈ [ idx[3][0], idx[3][1] ]
 *    - N ∈ [ idx[4][0], idx[4][1] ]
 *   last part of idx holds total length of ref/L and index of $(EOS) in L.
 *    - idx[5][0] = length of ref/L
 *    - idx[5][1] = index of $ in L.
 * - location is of the same length as there are parts in the genome used.
 *   (i.e. the human genome is made up of 26 pars: chromosone 1-22, X, Y, MT
 *   and the nonchromosonial(the parts we haven't been able to map yet)).
 *   Each index in location stores the index in ref/refb where that
 *   (non)chromosone begins.
 *
 * The compression is 2+1 bit:
 *  A = 0b00/0b0 (ref/refb)
 *  C = 0b01/0b0 (ref/refb)
 *  G = 0b10/0b0 (ref/refb)
 *  T = 0b11/0b0 (ref/refb)
 *  N = 0b00/0b1 (ref/refb)
 *  $ = 0b00/0b0 (ref/refb) Note the last entry in idx!!!
 *
 * ref and L will be padded with 0's at the end if(length_of_ref%compression(4))
 * refb and L will be padded with 0's at the end if(length_of_ref%8))
 *
 *
 *   EXAMPLE:
 *   Content of first file:
 *   >HEADER STRING FOR THIS FILE: - WILL BE DISREGARDED XXX: Could implement a way to read the chromosone number and thusly allow random input order.
 *   CANT
 *
 *   Content of second file:
 *   >ANOTHER HEADER STRING - ALSO DISREGARDED.
 *   GAT
 *
 *   Gives ref-sequence: CANTGAT$ ($ added at the end as a termination sign).
 *
 *   BW-Matrix:
 *   $CANTGAT
 *   ANTGAT$C
 *   AT$CANTG
 *   CANTGAT$
 *   GAT$CANT
 *   NTGAT$CA
 *   T$CANTGA
 *   TGAT$CAN
 *
 *   Content of filled fmindex:
 *   ref    0b0100001110001100
 *   refb   0b 0 0 1 0 0 0 0 0
 *
 *   L      0b1101100011000000
 *   Lb     0b 0 0 0 0 0 0 0 1
 *
 *   orig    { 7              } (compressed version in the fmindex)
 *   orig    { 7 1 5 0 4 2 6 3} (uncompressed version, stored only if rov set to 1)
 *
 *   Rank: Here shown with L and orig as two first collumns.
 *
 *           rank(as stored) rank(all values)
 *   i) L O |   A C G T N   |   A C G T N   |
 *   ----------------------------------------
 *   0) T 7 |   0 0 0 1 0   |   0 0 0 1 0   |
 *   1) C - |   - - - - -   |   0 1 0 1 0   |
 *   2) G - |   - - - - -   |   0 1 1 1 0   |
 *   3) $ - |   - - - - -   |   0 1 1 1 0   |
 *   4) T - |   - - - - -   |   0 1 1 2 0   |
 *   5) A - |   - - - - -   |   1 1 1 2 0   |
 *   6) A - |   - - - - -   |   2 1 1 2 0   |
 *   7) N - |   - - - - -   |   2 1 1 2 1   |
 *
 *          [0] [1]
 *   idx[0]  1   2  index of first/last A in first BW-Matrix collumn
 *   idx[1]  3   3  index of first/last C in first BW-Matrix collumn
 *   idx[2]  4   4  index of first/last G in first BW-Matrix collumn
 *   idx[3]  6   7  index of first/last T in first BW-Matrix collumn
 *   idx[4]  5   5  index of first/last N in first BW-Matrix collumn
 *   idx[5]  8   3  # of letters in ref / index of eol in L
 *
 *   location length 2:
 *   location[0] = 0
 *   location[1] = 4
 */
typedef struct fm_index {
    char *ref;
    char *refb;
    char *L;
    char *Lb;
    unsigned int *orig;
    unsigned int **rank;
    unsigned int **idx;
    unsigned int *location;
} fmindex;

/* Round_up(a/b); */
unsigned int iceil(unsigned int a, unsigned int b);

/* Compress the reference sequence*/
char *compress_ref(char *dst, char *src, unsigned int length);

/* Compress the L collumn, Orig & setting fmi->idx[5][0](L->EOS('$') index)*/
char *compress_L(char *dst, char *src, unsigned int length);

/* Retrieve comp chars compressed within c and stores them in output. */
void extract(char *output, char c, char n);

/* Initialize the fmi-struct; */
void init_index(unsigned int length, unsigned int location_length);

/* Fill the global instanse of fmindex with content found in file */
void fill_index_file(FILE *file);

/* Fill the global instanse of fmindex with content found in char* */
void fill_index_char(unsigned int length, char *ref, char *L, unsigned int *location, unsigned int *SAI);

/* Free all internal pointers of global fmindex */
void kill_index();

/* Print content to screen, Debug purposes! */
void print_index();

/* Write index to file */
void index_to_file(FILE *file);

#endif
