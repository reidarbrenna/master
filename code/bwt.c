#include "bwt.h"

int compare(const void *pt1, const void *pt2) {
    return strcmp(&(ref[*(unsigned int *)pt1]),&(ref[*(unsigned int *)pt2]));
}

void merge(unsigned int A[], unsigned int B[], unsigned int m, unsigned int n) 
{
    unsigned int i=0, j=0, k=0, p;
    unsigned int size = m+n;
    unsigned int *C = (unsigned int *)malloc(size*sizeof(unsigned int));
    while (i < m && j < n) {
        if (strcmp(&(ref[A[i]]), &(ref[B[j]])) <= 0)
            C[k++] = A[i++];
        else
            C[k++] = B[j++];
    }
    if (i < m)
        for (p = i; p < m; p++,k++)
            C[k] = A[p];
    else
        for (p = j; p < n; p++,k++)
            C[k] = B[p];
    for (i = 0; i < size; i++)
        A[i] = C[i];
    free(C);
}

void merge_multiple(unsigned int *a, unsigned int s, unsigned int r, unsigned int N) {
    unsigned int i;
    unsigned int size = s;
    while (N > 1) {
#pragma omp parallel for //private(i)
        for(i = 0; i < N; i += 2) {
            if (i + 2 >= N)
                merge(&(a[i*size]),&(a[(i+1)*size]),size,size+r);
            else
                merge(&(a[i*size]),&(a[(i+1)*size]),size,size);
        }
        size *= 2;
        N /= 2;
    }
}

void sort_SAI(unsigned int threads) {
    unsigned int i, len = ref_size + 1;
    unsigned int t_size = len/threads;
    unsigned int t_rest = len%threads;
    SAI = (unsigned int *)malloc(len * sizeof(unsigned int));

#pragma omp parallel for
    for (i = 0; i < len; i++)
        SAI[i] = i;

#pragma omp parallel for
    for (i = 0; i < threads; i++) {
        if (i == threads - 1)
            qsort((void *)&(SAI[i*t_size]), t_size + t_rest, sizeof(unsigned int), compare);
        else
            qsort((void *)&(SAI[i*t_size]), t_size, sizeof(unsigned int), compare);
    }

    merge_multiple(SAI, t_size, t_rest, threads);

    L = (char *)malloc(len * sizeof(char));

#pragma omp parallel for
    for (i = 0; i < len; i++) {
        if (!SAI[i])
            L[i] = ref[len - 1];
        else
            L[i] = ref[SAI[i]-1];
    }
}
