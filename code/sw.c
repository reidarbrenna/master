#include "search.h"

int full_SW(storage_t *match, char *q, char *bm, int q_len, int r_len, int *H, int *E, unsigned int ref_index, int *score_index) {
    int i, j, h, e=0, f, s = 0;

    memset(E, 0, q_len*sizeof(int));
    memset(H, 0, q_len*r_len*sizeof(int));
    score_index[0] = score_index[1] = 0;

    for (i = 0; i < r_len ; i++) {
        f = 0;
        h = 0;
        for (j = 0; j < q_len; j++) {
            e = E[j];
            if ((fmi->refb[(ref_index + i)/8] >> (7 - ((ref_index + i)%8))) & 0x1)
                h += MISSMATCH;
            else
                h += score(q, bm, j, ((fmi->ref[(i + ref_index)/comp] >> ((comp - 1 - ((ref_index + i)%comp))*2)) & 0x3));

            if (e > h) h = e;
            if (f > h) h = f;
            if (h < 0) h = 0;
            if (h > s) {
                s = h;
                score_index[0] = i;
                score_index[1] = j;
            }

            H[(j * r_len) + i] = h;
            e -= GAP_EXTEND;
            f -= GAP_EXTEND;
            h -= GAP_OPEN_EXTEND;

            if (h > e) e = h;
            if (h > f) f = h;

            E[j] = e;
            if (i == 0)
                h = 0;
            else
                h = H[j*r_len + i-1];
        }
    }
    return s;
}

void create_CIGAR(sam_t *sam, int *H, int *score_index) {
    int i = score_index[0];
    int j = score_index[1];
    int R_LEN = Q_LEN + 2*SW_REF_PADDING;
    int c = 0;
    char tmp;
    int clip;

    int u, d, l;

    //Soft clipping:
    for (clip = 0; clip < Q_LEN-j-1; clip++)
        (sam->CIGAR)[c++] = 'S';

    while (i >= 0 && j >= 0) {
        if (!i && !j) {
            u = d = l = 0;
            (sam->CIGAR)[c++] = 'M';
            break;
        } else if (!i) {
            d = l = 0;
            u = H[(j-1)*R_LEN +  i   ];
        } else if (!j) {
            u = d = 0;
            l = H[ j   *R_LEN + (i-1)];
        } else {
            u = H[(j-1)*R_LEN +  i   ];
            d = H[(j-1)*R_LEN + (i-1)];
            l = H[ j   *R_LEN + (i-1)];
        }

        if (!u && !d && !l) {
            (sam->CIGAR)[c++] = 'M';
            break;
        }

        if (d >= u && d >= l) {
            (sam->CIGAR)[c++] = 'M';
            i--;
            j--;
        } else if (u >= d && u >= l) {
            (sam->CIGAR)[c++] = 'D';
            j--;
        } else if (l >= d && l >= u) {
            (sam->CIGAR)[c++] = 'I';
            i--;
        }
    }

    for (clip = j; clip > 0; clip--)
        (sam->CIGAR)[c++] = 'S';
    (sam->CIGAR)[c] = '\0';

    j = c-1;
    for (i = 0; i < c/2; i++, j--) {
        tmp = (sam->CIGAR)[i];
        (sam->CIGAR)[i] = (sam->CIGAR)[j];
        (sam->CIGAR)[j] = tmp;
    }
}
