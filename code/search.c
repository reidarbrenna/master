#include "search.h"
#include "io.h"
#ifndef MAC_COMPILED
#include "simd.h"
#endif

int score(char *q, char *bm, int q_index, char r) {
    char query = ((q[q_index/comp] >> ((comp - 1 - (q_index%comp))*2)) & 0x3);
    if (((bm[q_index/8] >> (7 - (q_index%8))) & 0x1) || (r =='N'))
        return MISSMATCH;
    else if (r == query)
        return MATCH;
    else 
        return MISSMATCH;
}

void prosses_query(char *dst_ori, char *dst_oriN, char *dst_rev, char *dst_revN, char *src, int length) {
    int i;

    for (i = 0; i < length; i++) {
        if (src[i] == 'A') {
            dst_rev[(length - 1 - i)/comp] |= (0x3 << ((comp - 1)-((length - 1 - i)%comp))*2);
        } else if (src[i] == 'C') {
            dst_ori[i/comp]                |= (0x1 << ((comp - 1)-(i%comp))*2);
            dst_rev[(length - 1 - i)/comp] |= (0x2 << ((comp - 1)-((length - 1 - i)%comp))*2);
        } else if (src[i] == 'G') {
            dst_ori[i/comp]                |= (0x2 << ((comp - 1)-(i%comp))*2);
            dst_rev[(length - 1 - i)/comp] |= (0x1 << ((comp - 1)-((length - 1 - i)%comp))*2);
        } else if (src[i] == 'T') {
            dst_ori[i/comp]                |= (0x3 << ((comp - 1)-(i%comp))*2);
        } else {
            dst_oriN[i/8]                  |= (0x1 << (7-(i%8)));
            dst_revN[(length - 1 - i)/8]   |= (0x1 << (7-((length - 1 - i)%8)));
        }
    }
}

void print_query(char *src, char *bm, int length) {
    int i;
    char flag = 0x0;
    char output[comp];
    for (i = 0; i < length; i++) {
        if (!(i%comp)) {
            if (flag) {
                extract(output, src[i/comp], bm[i/8]);
                flag = 0x0;
            } else {
                extract(output, src[i/comp], (char)(bm[i/8] >> 4));
                flag = 0x1;
            }
        }
        printf("%c",output[i%comp]);
    }
    printf("\n");
}

unsigned int get_rank(int c, unsigned int idx) {
    unsigned int count = 0;
    if (c == 4) {
        while (idx%rov) {
            if ((fmi->Lb[idx/8] >> (7 - (idx%8))) & 0x1)
                count++;
            idx--;
        }
    } else if (c == 0) {
        while (idx%rov) {
            if (idx == fmi->idx[5][1]) {
                idx--;
                continue;
            }
            if (!((fmi->L[idx/comp] >> ((comp - 1 - (idx%comp))*2)) & 0x3) && !((fmi->Lb[idx/8] >> (7 - (idx%8))) & 0x1))
                count++;
            idx--;
        }
    } else {
        while (idx%rov) {
            if ((int)(fmi->L[idx/comp] >> ((comp - 1 - (idx%comp))*2) & 0x3) == c)
                count++;
            idx--;
        }
    }
    return fmi->rank[c][idx/rov] + count;
}

unsigned int get_prev(unsigned int idx) {
    char l;
    if ((fmi->Lb[idx/8] >> (7 - (idx%8))) & 0x1)
        return fmi->idx[4][0] + get_rank(4, idx) - 1;
    l = (fmi->L[idx/comp] >> ((comp - 1 - (idx%comp))*2)) & 0x3;
    return fmi->idx[(int)l][0] + get_rank(l, idx) - 1;
}

unsigned int get_orig(unsigned int idx, unsigned int carry) {
    if (idx == fmi->idx[5][1])
        return carry;
    if (!(idx%rov))
        return fmi->orig[idx/rov] + carry;
    return get_orig(get_prev(idx),++carry);
}

int minimum(unsigned int a, unsigned int b) {
    if (a < b)
        return a;
    return b;
}

void add_to_storage(storage_t *sp, storage_t **high, storage_t **low, int *sp_idx, int length, int query_index, char rev_flag, unsigned int ref_range_start, unsigned int ref_range_stop, int ql) {
    unsigned int i = ref_range_start;
    if (*sp_idx == MAX_STORE) {
        while (((*low)->length < length) && (i <= ref_range_stop)) {
            (*high) = add_new(sp, (*high), low, sp_idx, length, get_orig(i++,0), query_index, rev_flag, ql);
        }
    } else {
        while ((*sp_idx != MAX_STORE) && (i <= ref_range_stop)) {
            (*high) = add_new(sp, (*high), low, sp_idx, length, get_orig(i++,0), query_index, rev_flag, ql);
        }
    }
}

void find_partial(char *query, char *bm, int query_length, char rev_flag, storage_t *sp, storage_t **low, storage_t **high, int *sp_idx) {
    int i, length = 0;
    char c;
    unsigned int St = 0, Ed = 0;
    unsigned int tmp_St = 0, tmp_Ed = 0;

    for (i = query_length - 1; i >= 0; i--) {

        /* Checking if the letter in query is an N. (They are unknown and
         * thusly unmatchable).
         * XXX: Will try to store if at least length_limit has been reached!.
         */
        if ((bm[i/8] >> (7 - (i%8))) & 0x1) {
            if (length >= length_limit) {
                add_to_storage(sp, high, low, sp_idx, length, i+1, rev_flag, St, Ed, query_length);
            }
            length = 0;
            continue;
        }
        c = ((query[i/comp] >> ((comp - 1 - (i%comp))*2)) & 0x3);
        if (!length) {
            tmp_St = fmi->idx[(int)c][0];
            tmp_Ed = fmi->idx[(int)c][1];
        } else {
            tmp_St = fmi->idx[(int)c][0] + get_rank(c,St - 1);
            tmp_Ed = fmi->idx[(int)c][0] + get_rank(c,Ed) - 1;
        }

        if (tmp_St > tmp_Ed) {
            if (length >= length_limit) {
                add_to_storage(sp, high, low, sp_idx, length, i+1, rev_flag, St, Ed, query_length);
                i++;
                length = 0;
            }
        } else {
            St = tmp_St;
            Ed = tmp_Ed;
            length++;
        }
    }
    if (length >= length_limit) {
        add_to_storage(sp, high, low, sp_idx, length, 0, rev_flag, St, Ed, query_length);
    }
}
void set_RNAME(sam_t *sam, unsigned int ref_sequence) {
    switch (ref_sequence) {
        case 1:
            strncpy(sam->RNAME, "C01", 3);
            break;
        case 2:
            strncpy(sam->RNAME, "C02", 3);
            break;
        case 3:
            strncpy(sam->RNAME, "C03", 3);
            break;
        case 4:
            strncpy(sam->RNAME, "C04", 3);
            break;
        case 5:
            strncpy(sam->RNAME, "C05", 3);
            break;
        case 6:
            strncpy(sam->RNAME, "C06", 3);
            break;
        case 7:
            strncpy(sam->RNAME, "C07", 3);
            break;
        case 8:
            strncpy(sam->RNAME, "C08", 3);
            break;
        case 9:
            strncpy(sam->RNAME, "C09", 3);
            break;
        case 10:
            strncpy(sam->RNAME, "C10", 3);
            break;
        case 11:
            strncpy(sam->RNAME, "C11", 3);
            break;
        case 12:
            strncpy(sam->RNAME, "C12", 3);
            break;
        case 13:
            strncpy(sam->RNAME, "C13", 3);
            break;
        case 14:
            strncpy(sam->RNAME, "C14", 3);
            break;
        case 15:
            strncpy(sam->RNAME, "C15", 3);
            break;
        case 16:
            strncpy(sam->RNAME, "C16", 3);

            break;
        case 17:
            strncpy(sam->RNAME, "C17", 3);
            break;
        case 18:
            strncpy(sam->RNAME, "C18", 3);
            break;
        case 19:
            strncpy(sam->RNAME, "C19", 3);
            break;
        case 20:
            strncpy(sam->RNAME, "C20", 3);
            break;
        case 21:
            strncpy(sam->RNAME, "C21", 3);
            break;
        case 22:
            strncpy(sam->RNAME, "C22", 3);
            break;
        case 23:
            strncpy(sam->RNAME, "CMT", 3);
            break;
        case 24:
            strncpy(sam->RNAME, "C_Y", 3);
            break;
        case 25:
            strncpy(sam->RNAME, "C_X", 3);
            break;
        default:
            strncpy(sam->RNAME, "NON", 3);
    }
}

void search(FILE *fp, sam_t *sam, int m) {
    int j;
    int i = 0, len = Q_LEN;
    int overflow;
    char **refs;
    storage_t *sp = (storage_t *)malloc(MAX_STORE*sizeof(storage_t));
    char **query = (char **)malloc(4*sizeof(char *));
    query[0] = (char *)malloc(iceil(len,comp) * sizeof(char));
    query[1] = (char *)malloc(iceil(len,8)    * sizeof(char));
    query[2] = (char *)malloc(iceil(len,comp) * sizeof(char));
    query[3] = (char *)malloc(iceil(len,8)    * sizeof(char));
    storage_t *low = NULL;
    storage_t *high = NULL;
    memset(query[0], 0, iceil(len, comp)*sizeof(char));
    memset(query[1], 0, iceil(len,    8)*sizeof(char));
    memset(query[2], 0, iceil(len, comp)*sizeof(char));
    memset(query[3], 0, iceil(len,    8)*sizeof(char));

    printf("Processing query\n");
    prosses_query(query[0], query[1], query[2], query[3], sam->SEQ, len);

    //part 3: find the N best partial matches.
    //Find partial hit(s) with the original query.
    printf("Looking for partials in original query\n");
    find_partial(query[0], query[1], len, 0x0, sp, &low, &high, &i);
    //Find partial hit(s) with the reverse complimented query.
    printf("Looking for partials in reversed query\n");
    find_partial(query[2], query[3], len, 0x1, sp, &low, &high, &i);

    //part 4: sort matches and create a merged ref sequence:
    storage_t *tmp = high;
    while (tmp != NULL) {
        printf("Score: %d|%d\n",tmp->score,tmp->length);
        tmp = tmp->next;
    }

    if (high->score) {
        unsigned int ref_sequence = fmi->location[0];
        for (j = 2; j <= fmi->location[0]; j++) {
            if (high->ref_index < fmi->location[j]) {
                ref_sequence = j - 1;
                break;
            }
        }

        set_RNAME(sam, ref_sequence);
        sam->POS = high->ref_index - fmi->location[ref_sequence];
        memset(sam->CIGAR, 'M', Q_LEN*sizeof(char));
        (sam->CIGAR)[Q_LEN] = '\0';

        printf("PERFECT MATCH!\n");
        memset(sam->CIGAR,'M',Q_LEN);
        sam->CIGAR[Q_LEN] = '\0';
        write_to_file(fp,sam,high,Q_LEN);
    } else {
        printf("Sorting storage based on length...\n");
        sort_storage_length(sp,i);
        printf("Creating partial refs...\n");
        overflow = create_partial_refs(&refs, sp, i);

        printf("SW on partial refs\n");
#pragma omp parallel for
        for (j = 0; j < i-overflow; j+=SW_MULTIPLE) {
            __mxxxi *HE = (__mxxxi *)malloc(2*Q_LEN*sizeof(__mxxxi));
            sw_simd(HE,sp,query,len,SW_MULTIPLE,j,refs[j/SW_MULTIPLE]);
            free(HE);
        }
        if (overflow) {
            __mxxxi *HE = (__mxxxi *)malloc(2*Q_LEN*sizeof(__mxxxi));
            sw_simd(HE,sp,query,len,overflow,j,refs[j/SW_MULTIPLE]);
            free(HE);
        }

        printf("Sorting storage based on score\n");
        sort_storage_score(sp, i);
        for (j = 0; j < m; j++) {
            int __attribute__((aligned(64))) I[2];
            int *H = (int *)malloc(Q_LEN*(Q_LEN + 2*SW_REF_PADDING)*sizeof(int));
            int *E = (int *)malloc(Q_LEN*sizeof(int));

            printf("Running a FULL SW to optain CIGAR string...\n");
            if (sp[j].reversed_flag)
                full_SW(&(sp[j]),query[2],query[3],Q_LEN, Q_LEN + (2 * SW_REF_PADDING), H, E, sp[j].ref_index, I);
            else
                full_SW(&(sp[j]),query[0],query[1],Q_LEN, Q_LEN + (2 * SW_REF_PADDING), H, E, sp[j].ref_index, I);

            create_CIGAR(sam, H, I);
            printf("%s\n",sam->CIGAR);
            set_RNAME(sam, sp[j].query_index);
            sam->POS = sp[j].ref_index - sp[j].query_index;
            write_to_file(fp,sam,&(sp[j]),Q_LEN);
            free(H);
            free(E);
        }
        for (j = 0; j < iceil(i,SW_MULTIPLE); j++)
            free((refs)[j]);
        free(refs);
    }

    free(sp);
    free(query[0]);
    free(query[1]);
    free(query[2]);
    free(query[3]);
    free(query);
}
