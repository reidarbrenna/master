#include "storage.h"

int compare_storage_length(const void *pt1, const void *pt2) {
    return ((storage_t *)pt2)->length - ((storage_t *)pt1)->length;
}

int compare_storage_score(const void *pt1, const void *pt2) {
    return ((storage_t *)pt2)->score - ((storage_t *)pt1)->score;
}

void sort_storage_length(storage_t *storage, int stored) {
    return qsort((void *)storage, stored, sizeof(storage_t), compare_storage_length);
}

void sort_storage_score(storage_t *storage, int stored) {
    return qsort((void *)storage, stored, sizeof(storage_t), compare_storage_score);
}

/* Traverse the storage list and place the new match in such a way that the list
 * is sorted according to match length (high to low).
 */
storage_t *fill(storage_t *high, storage_t *cur, storage_t **low) {
    storage_t *tmp = high;
    while(1) {
        if (tmp->length >= cur->length) {
            if (tmp->next == NULL) {
                tmp->next = cur;
                cur->prev = tmp;
                (*low) = cur;
                break;
            }
            tmp = tmp->next;
        } else {
            if (tmp->prev == NULL) {
                cur->next = tmp;
                tmp->prev = cur;
                return cur;
            }
            tmp->prev->next = cur;
            cur->next = tmp;
            cur->prev = tmp->prev;
            tmp->prev = cur;
            break;
        }
    }
    return high;
}

/* Called by user to potentially store a newly found match */
storage_t *add_new(storage_t *storage_space, storage_t *high, storage_t **low, int *storage_idx, int length, unsigned int ref_index, int query_index, char rev_flag, int ql) {
    storage_t *cur;
    //long i;

    /* Checking if a match found span multiple chromosones.
     * Or at least the end of one and the beginning of another.
     * This is now allowed and a such a match will thusly be scrapped.
     */
    //for (i = 2; i <= (fmi->location)[0]; i++) {
    //    if ((ref_index < fmi->location[i]) && (ref_index + (long)length - 1) >= fmi->location[i]) {
    //        return high;
    //    }
    //}

    /* Checking if we have reached our storage cappasity.
     * if so, one of two things will happen:
     *  1) if the new match has a lower length than the lowest in our list we
     *     scrapp it.
     *  2) if the new is longer on the other hand, we delete the previously low
     *     and utilize the newly aquired space to store our new match.
     */
    if ((*storage_idx) == MAX_STORE) {
        if ((*low)->length >= length) {
            return high;
        }
        cur = *low;
        (*low) = (*low)->prev;
        (*low)->next = NULL;
    } else {
        cur = &(storage_space[(*storage_idx)++]);
    }

    /* Filling a storage_t struct with content */
    cur->length = length;
    cur->ref_index = ref_index;
    cur->query_index = query_index;
    cur->next = NULL;
    cur->prev = NULL;
    cur->reversed_flag = rev_flag;
    if (length == ql)
        cur->score = ql;
    else
        cur->score = 0;

    /* If this is the first match stored we don't need to traverse the
     * (non-existing list to place the new match).
     * Else, we procede to fill in order to place the new match.
     */
    if ((*storage_idx) == 1) {
        (*low) = cur;
        return cur;
    }

    /* If cur->length <= low->length we know that cur is the new low. */
    if ((*low)->length >= length) {
        cur->prev = (*low);
        (*low)->next = cur;
        (*low) = cur;
        return high;
    }
    return fill(high, cur, low);
}

/* DEBUG purpose only...
 * Prints the content of a match alligned to the ref in order to check content
 */
void print_storage(storage_t *tmp, char *o, char *bm_o, char *r, char *bm_r, int q_len) {
    long i;
    long j = 2;
    char c;
    char *q, *bm;

    if (tmp->reversed_flag) {
        printf("\nAlligned from the reverse-complimented query!\n");
        q = r;
        bm = bm_r;
    } else {
        printf("\nAlligned from the un-altered (original) query!\n");
        q = o;
        bm = bm_o;
    }

    if (tmp->score)
        printf("Match length: PERFECT MATCH!!!\n");
    else
        printf("Match length: %d characters.\n",tmp->length);

    printf("ref   | ");
    if (tmp->ref_index < (long)(tmp->query_index)) {
        for (i = tmp->ref_index; i != (long)(tmp->query_index); i++)
            printf(" ");
    }

    for (i = 0; i < fmi->idx[5][0] - 1; i++) {
        if ((j <= fmi->location[0]) && (i+1 == fmi->location[j])) {
            printf(" ");
            j++;
            continue;
        }
        if ((fmi->refb[i/8] >> (7-(i%8)))&0x1) {
            printf("N");
            continue;
        }
        c = (fmi->ref[i/comp] >> ((comp - 1 - (i%comp))*2)) & 0x3;
        if (c == 1)
            c = 'C';
        else if (c == 2)
            c = 'G';
        else if (c == 3)
            c = 'T';
        else
            c = 'A';
        printf("%c",c);
    }
    printf("$\n");

    printf("        ");
    if (tmp->ref_index < (long)(tmp->query_index)) {
        for (i = tmp->ref_index; i != (long)(tmp->query_index); i++)
            printf(" ");
    }
    for (i = 0; i < tmp->ref_index; i++)
        printf(" ");
    for (i = 0; i < tmp->length; i++) {
        printf("|");
    }
    printf("\n");

    printf("query | ");
    for (i = 0; i < tmp->ref_index - (long)tmp->query_index; i++)
        printf(" ");
    for (i = 0; i < (long)q_len; i++) {
        if ((bm[i/8] >> (7-(i%8)))&0x1) {
            printf("N");
            continue;
        }
        c = (q[i/comp] >> ((comp - 1 - (i%comp))*2)) & 0x3;
        if (c == 1)
            c = 'C';
        else if (c == 2)
            c = 'G';
        else if (c == 3)
            c = 'T';
        else
            c = 'A';
        printf("%c",c);
    }
    printf("\n");
}
