#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    FILE *xeon = fopen(argv[1],"r");
    char *xr = NULL;
    char *pr = NULL;
    FILE *phi  = fopen(argv[2],"r");
    char x[500], p[500];
    int c = 0;

    while (1) {
        xr = fgets(x,500,xeon);
        pr = fgets(p,500,phi);
        if (xr == NULL || pr == NULL)
            break;
        if (strcmp(x,p) != 0) {
            printf("Difference occured:\n");
            printf("\t@line %d\n",c);
        }
        ++c;
    }
    if (xr != NULL || pr != NULL)
        printf("Files have different length!?!\n");
    return 0;
}
